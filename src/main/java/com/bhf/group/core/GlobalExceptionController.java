package com.bhf.group.core;

import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.net.ConnectException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@RestControllerAdvice
public class GlobalExceptionController {

    @ExceptionHandler(value = Exception.class)
    public RestResult<Object> exceptionHandler(Exception ex) {
        ex.printStackTrace();
        if (ex instanceof ConnectException) {
            return RestResultUtil.buildResult(ResultCodeEnum.CONNECT_TIMEOUT_ERROR);
        }
        if (ex instanceof HttpRequestMethodNotSupportedException) {
            return RestResultUtil.buildResult(ResultCodeEnum.NOT_SUPPORTED_REQUEST_METHOD);
        }
        if (ex instanceof HttpMessageNotReadableException) {
            return RestResultUtil.buildResult(ResultCodeEnum.REQUEST_MISSING_BODY_ERROR);
        }
        return RestResultUtil.buildResult(ResultCodeEnum.ERROR, ex.getMessage());
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public RestResult<Object> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        List<FieldError> allErrors = e.getBindingResult().getFieldErrors();
        final Map<String, String> collect = allErrors.stream().collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage, (key1, key2) -> key2, LinkedHashMap::new));
        return RestResultUtil.buildResult(ResultCodeEnum.ARGUMENT_VALID_ERROR, collect);
    }
}
