package com.bhf.group.core;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author pwq
 */
@Getter
@Setter
public class Page implements Serializable {

    @Builder.Default
    private int pages = 1;
    @Builder.Default
    private int size = 10;
}
