package com.bhf.group.core;

import org.apache.lucene.analysis.*;
import org.apache.lucene.analysis.cn.smart.HMMChineseTokenizer;
import org.apache.lucene.analysis.en.PorterStemFilter;

import java.util.List;

/**
 * @author pwq
 */
public class CustomSmartChineseAnalyzer extends Analyzer {

    private CharArraySet extendWords;
    private final List<String> words;
    private final CharArraySet stopWords;

    public CustomSmartChineseAnalyzer(CharArraySet stopWords, List<String> words) {
        this.stopWords = stopWords;
        this.words = words;
    }

    @Override
    public Analyzer.TokenStreamComponents createComponents(String fieldName) {
        final Tokenizer tokenizer = new HMMChineseTokenizer();
        TokenStream result = tokenizer;
        result = new LowerCaseFilter(result);
        result = new PorterStemFilter(result);

        if (!stopWords.isEmpty()) {
            result = new StopFilter(result, stopWords);
        }
        if (!words.isEmpty()) {
            result = new ExtendWordFilter(result, words);
        }
        return new TokenStreamComponents(tokenizer, result);
    }

    @Override
    protected TokenStream normalize(String fieldName, TokenStream in) {
        return new LowerCaseFilter(in);
    }
}
