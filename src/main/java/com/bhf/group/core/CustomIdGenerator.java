package com.bhf.group.core;

import com.baomidou.mybatisplus.core.incrementer.DefaultIdentifierGenerator;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author pwq
 */
public class CustomIdGenerator implements IdentifierGenerator {
    @Override
    public Long nextId(Object entity) {
        DefaultIdentifierGenerator identifierGenerator = null;
        try {
            identifierGenerator = new DefaultIdentifierGenerator(InetAddress.getByName(InetAddress.getLocalHost().getHostAddress()));
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        assert identifierGenerator != null;
        return identifierGenerator.nextId(entity);
    }
}

