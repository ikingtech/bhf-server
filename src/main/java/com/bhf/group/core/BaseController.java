package com.bhf.group.core;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.bhf.group.security.entity.UserInfo;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Map;

/**
 * @author pwq
 */
public class BaseController {
    /**
     * 获取用户的基本信息
     *
     * @return
     */
    public UserInfo getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Map<String, Object> map = BeanUtil.beanToMap(authentication.getPrincipal());
        return BeanUtil.mapToBean(map, UserInfo.class, false, CopyOptions.create());
    }
}
