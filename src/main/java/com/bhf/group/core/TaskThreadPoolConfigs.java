package com.bhf.group.core;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 线程池配置属性类
 *
 * @author pwq
 */
@Data
@Component
@ConfigurationProperties(prefix = "task.thread-pool")
public class TaskThreadPoolConfigs {

    /**
     * 核心线程数
     */
    private int corePoolSize = 5;

    /**
     * 最大线程数
     */
    private int maxPoolSize = 10;

    /**
     * 线程空闲时间
     */
    private int keepAliveSeconds = 3000;

    /**
     * 任务队列容量（阻塞队列）
     */
    private int queueCapacity = Integer.MAX_VALUE;
}
