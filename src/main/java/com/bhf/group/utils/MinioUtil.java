package com.bhf.group.utils;

import cn.hutool.core.util.IdUtil;
import cn.hutool.crypto.SecureUtil;
import io.minio.*;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
@Component
public class MinioUtil {

    @Resource
    private MinioClient minioClient;

    /**
     * 创建一个新的存储桶
     *
     * @param bucket 名称
     */
    public void createBucket(String bucket) throws Exception {
        if (!bucketExists(bucket)) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucket).build());
        }
    }

    /**
     * 列出所有存储桶。
     *
     * @return
     */
    public List<Bucket> listBuckets() throws Exception {
        return minioClient.listBuckets(ListBucketsArgs.builder().build());
    }

    /**
     * 检查存储桶是否存在。 true if the bucket exists
     *
     * @return boolean
     */
    public boolean bucketExists(String bucketName) throws Exception {
        return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
    }

    /**
     * 删除一个存储桶。
     *
     * @param bucketName
     */
    public void removeBucket(String bucketName) throws Exception {
        if (bucketExists(bucketName)) {
            minioClient.removeBucket(RemoveBucketArgs.builder().bucket(bucketName).build());
        }
    }

    /**
     * 列出存储桶中被部分上传的对象。
     *
     * @param bucketName  存储桶名称。
     * @param prefix      对象名称的前缀
     * @param recursive   是否递归查找，如果是false,就模拟文件夹结构查找。
     * @param useVersion1 如果是true, 使用版本1 REST API
     * @return
     */
    public Iterable<Result<Item>> listObjects(String bucketName, String prefix, boolean recursive, boolean useVersion1) throws Exception {
        if (bucketExists(bucketName)) {
            return minioClient.listObjects(ListObjectsArgs.builder().bucket(bucketName)
                    .prefix(prefix).recursive(recursive).useApiVersion1(useVersion1).build());
        }
        return null;
    }

    /**
     * 获得指定对象前缀的存储桶策略。
     *
     * @param bucketName
     * @return
     */
    public String getBucketPolicy(String bucketName) throws Exception {
        if (bucketExists(bucketName)) {
            return minioClient.getBucketPolicy(GetBucketPolicyArgs.builder().bucket(bucketName).build());
        }
        return null;
    }

    /**
     * 给一个存储桶+对象前缀设置策略。
     *
     * @param bucketName
     * @param config     json 格式 详细请看文档
     */
    public void setBucketPolicy(String bucketName, String config) throws Exception {
        if (bucketExists(bucketName)) {
            minioClient.setBucketPolicy(SetBucketPolicyArgs.builder().bucket(bucketName).config(config).build());
        }
    }

    /**
     * 一个对象复制数据来创建一个对象
     *
     * @param bucketName
     * @param objectName
     * @param sourceName
     * @param sourceBucketName
     */
    public void copyObject(String bucketName, String objectName, String sourceName, String sourceBucketName) throws Exception {
        minioClient.copyObject(CopyObjectArgs.builder().bucket(bucketName).object(objectName)
                .source(CopySource.builder().object(sourceName).bucket(sourceBucketName).build()).build());
    }

    /**
     * 上传文件
     *
     * @param bucketName
     * @param objectName  /upload/2021/02/demo.jpg
     * @param stream
     * @param contentType
     */
    public String putObject(String bucketName, String objectName, InputStream stream, String contentType) throws Exception {
        if (bucketExists(bucketName)) {
            final ObjectWriteResponse objectWriteResponse = minioClient.putObject(
                    PutObjectArgs.builder().bucket(bucketName).object(objectName).stream(stream, stream.available(), -1).contentType(contentType).build());
            return objectWriteResponse.object();
        }
        return null;
    }

    /**
     * 上传文件
     *
     * @param bucketName
     * @param objectName /upload/2021/02/demo.jpg
     * @param file
     */
    public String putObject(String bucketName, String objectName, MultipartFile file) throws Exception {
        if (bucketExists(bucketName)) {
            final PutObjectArgs build = PutObjectArgs.builder().bucket(bucketName)
                    .contentType(file.getContentType()).object(objectName)
                    .stream(file.getInputStream(), file.getInputStream().available(), -1).build();
            final ObjectWriteResponse objectWriteResponse = minioClient.putObject(build);
            return objectWriteResponse.object();
        }
        return null;
    }

    /**
     * 上传文件
     *
     * @param bucketName
     * @param file
     * @return
     * @throws Exception
     */
    public String putObject(String bucketName, MultipartFile file) throws Exception {
        if (bucketExists(bucketName)) {
            final String year = SecureUtil.md5(String.valueOf(LocalDateTime.now().getYear()));
            final String month = SecureUtil.md5(String.valueOf(LocalDateTime.now().getMonthValue()));
            String originalFileName = file.getOriginalFilename();
            assert originalFileName != null;
            final String substring = originalFileName.substring(originalFileName.lastIndexOf("."));
            String path = year + "/" + month + "/" + IdUtil.objectId() + substring;
            final PutObjectArgs build = PutObjectArgs.builder().bucket(bucketName)
                    .contentType(file.getContentType()).object(path)
                    .stream(file.getInputStream(), file.getInputStream().available(), -1).build();
            final ObjectWriteResponse objectWriteResponse = minioClient.putObject(build);
            return "/" + bucketName + "/" + objectWriteResponse.object();
        }
        return null;
    }

    public Map<String, Object> putObjects(String bucketName, MultipartFile[] files) throws Exception {
        if (bucketExists(bucketName)) {
            Map<String, Object> map = new HashMap<>(3);
            for (int i = 0; i < files.length; i++) {
                final String year = SecureUtil.md5(String.valueOf(LocalDateTime.now().getYear()));
                final String month = SecureUtil.md5(String.valueOf(LocalDateTime.now().getMonthValue()));
                String originalFileName = files[i].getOriginalFilename();
                assert originalFileName != null;
                final String substring = originalFileName.substring(originalFileName.lastIndexOf("."));
                String path = year + "/" + month + "/" + IdUtil.objectId() + substring;
                final PutObjectArgs build = PutObjectArgs.builder().bucket(bucketName)
                        .contentType(files[i].getContentType()).object(path)
                        .stream(files[i].getInputStream(), files[i].getInputStream().available(), -1).build();
                final ObjectWriteResponse objectWriteResponse = minioClient.putObject(build);
                map.put(String.valueOf(i), "/" + bucketName + "/" + objectWriteResponse.object());
            }
            return map;
        }
        return null;
    }


    /**
     * 删除文件
     *
     * @param bucketName
     * @param objectName
     * @throws Exception
     */
    public void removeObject(String bucketName, String objectName) throws Exception {
        if (bucketExists(bucketName)) {
            minioClient.removeObject(RemoveObjectArgs.builder().bucket(bucketName).object(objectName).build());
        }
    }

    /**
     * 下载文件
     *
     * @param bucketName
     * @param objectName
     * @return
     * @throws Exception
     */
    public InputStream getObject(String bucketName, String objectName) throws Exception {
        if (bucketExists(bucketName)) {
            final GetObjectArgs build = GetObjectArgs.builder().bucket(bucketName).object(objectName).build();
            return minioClient.getObject(build);
        }
        return null;
    }
}

