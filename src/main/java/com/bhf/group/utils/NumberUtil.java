package com.bhf.group.utils;

import java.text.DecimalFormat;

/**
 * @author pwq
 */
public class NumberUtil {

    public static String getUnitOfNumber(Integer num) {
        if (num < 10000) {
            return String.valueOf(num);
        } else {
            double n = (double) num / 10000;
            DecimalFormat df = new DecimalFormat("#.0");
            return df.format(n) + "万";
        }
    }

    public static String getUnitOfNumber(long number) {
        if (number < 10000) {
            return String.valueOf(number);
        } else {
            double n = (double) number / 10000;
            DecimalFormat df = new DecimalFormat("#.0");
            return df.format(n) + "万";
        }
    }
}
