package com.bhf.group.utils;

import cn.hutool.core.util.NumberUtil;
import com.bhf.group.constants.Constants;
import com.bhf.group.core.CustomSmartChineseAnalyzer;
import com.bhf.group.entity.SysExtendWord;
import com.bhf.group.entity.SysStopWord;
import com.bhf.group.service.SysExtendWordService;
import com.bhf.group.service.SysStopWordService;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.CharArraySet;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@Component
public class TokenizerUtil {

    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private SysStopWordService stopWordService;
    @Resource
    private SysExtendWordService sysExtendWordService;

    public List<String> smartChineseAnalyzer(String text) throws IOException {
        List<String> stopWords;
        List<String> extendWords;
        List<String> resut = redisTemplate.opsForList().range(Constants.STOP_KEWS, 0, -1);
        if (Objects.nonNull(resut) && resut.size() > 0) {
            stopWords = resut;
        } else {
            final List<SysStopWord> list = stopWordService.list();
            stopWords = list.stream().map(SysStopWord::getWord).collect(Collectors.toList());
            redisTemplate.opsForList().rightPushAll(Constants.STOP_KEWS, stopWords);
        }
        List<String> extend = redisTemplate.opsForList().range(Constants.EXTEND_KEWS, 0, -1);
        if (Objects.nonNull(extend) && extend.size() > 0) {
            extendWords = extend;
        } else {
            final List<SysExtendWord> list = sysExtendWordService.list();
            extendWords = list.stream().map(SysExtendWord::getWord).collect(Collectors.toList());
            redisTemplate.opsForList().rightPushAll(Constants.EXTEND_KEWS, extendWords);
        }
        CharArraySet stops = new CharArraySet(stopWords.size(), true);
        stops.addAll(stopWords);
        Analyzer analyzer = new CustomSmartChineseAnalyzer(stops, extendWords);
        TokenStream tokenStream = analyzer.tokenStream("name", text);
        OffsetAttribute offsetAttribute = tokenStream.addAttribute(OffsetAttribute.class);
        tokenStream.reset();
        List<String> tokens = new ArrayList<>();
        while (tokenStream.incrementToken()) {
            if (offsetAttribute.toString().length()>1){
                if (!NumberUtil.isNumber(offsetAttribute.toString())){
                    tokens.add(offsetAttribute.toString());
                }
            }
        }
        tokenStream.end();
        return tokens;
    }
}

