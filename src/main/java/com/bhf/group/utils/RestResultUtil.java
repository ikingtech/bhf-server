package com.bhf.group.utils;

import com.bhf.group.constants.ResultCodeEnum;

/**
 * @author pwq
 */
public class RestResultUtil {
    public static <T> RestResult<T> success() {
        return RestResult.<T>builder().withCode(200).withMsg("请求成功").build();
    }

    public static <T> RestResult<T> success(T data) {
        return RestResult.<T>builder().withCode(200).withMsg("请求成功").withData(data).build();
    }

    public static <T> RestResult<T> success(String msg, T data) {
        return RestResult.<T>builder().withCode(200).withMsg(msg).withData(data).build();
    }

    public static <T> RestResult<T> success(int code, T data) {
        return RestResult.<T>builder().withCode(code).withData(data).build();
    }

    public static <T> RestResult<T> failed() {
        return RestResult.<T>builder().withCode(500).withMsg("服务器内部错误").build();
    }

    public static <T> RestResult<T> failed(String errMsg) {
        return RestResult.<T>builder().withCode(500).withMsg(errMsg).build();
    }

    public static <T> RestResult<T> failed(int code, T data) {
        return RestResult.<T>builder().withCode(code).withData(data).build();
    }

    public static <T> RestResult<T> failed(int code, T data, String errMsg) {
        return RestResult.<T>builder().withCode(code).withData(data).withMsg(errMsg).build();
    }

    public static <T> RestResult<T> failedWithMsg(int code, String errMsg) {
        return RestResult.<T>builder().withCode(code).withMsg(errMsg).build();
    }

    public static <T> RestResult<T> buildResult(ResultCodeEnum resultCode, T data) {
        return RestResult.<T>builder().withCode(resultCode.getCode()).withMsg(resultCode.getMsg()).withData(data).build();
    }

    public static <T> RestResult<T> buildResult(ResultCodeEnum resultCode) {
        return RestResult.<T>builder().withCode(resultCode.getCode()).withMsg(resultCode.getMsg()).build();
    }

}
