package com.bhf.group.utils;

import cn.hutool.core.util.IdUtil;
import org.springframework.stereotype.Component;

/**
 * @author pwq
 */
@Component
public class Snowflake {

    public static final Long WORKER_ID = 3L;
    public static final Long DATACENTER_ID = 5L;

    public String sid() {
        return String.valueOf(IdUtil.getSnowflake(WORKER_ID, DATACENTER_ID).nextId());
    }

    public Long lid() {
        return IdUtil.getSnowflake(WORKER_ID, DATACENTER_ID).nextId();
    }
}
