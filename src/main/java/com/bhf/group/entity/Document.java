package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author pwq
 */
@Data
@TableName("document")
public class Document implements Serializable {

    @NotBlank(message = "id不能为空", groups = {DocumentEdit.class})
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    @NotBlank(message = "名称不能为空", groups = {DocumentAdd.class, DocumentEdit.class})
    private String title;
    private String userId;
    private String keyWords;
    private Long likes = 0L;
    private Long collects = 0L;
    @NotBlank(message = "密级不能为空", groups = {DocumentAdd.class, DocumentEdit.class})
    private String level;
    private String summary;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    private Integer status = 0;
    @NotBlank(message = "分类id不能为空", groups = {DocumentAdd.class, DocumentEdit.class})
    private String documentCategoryId;
    @NotBlank(message = "模板id不能为空", groups = {DocumentAdd.class, DocumentEdit.class})
    private String templateId;
    private Integer approvalStatus = 0;
    private String documentCategoryParentId;
    private LocalDateTime publishTime;
    private Long previews;
    private String coverImage;
    private int isFreeze;

    @TableField(exist = false)
    private List<DocumentAnnex> documentAnnexList;
    @TableField(exist = false)
    private List<DocumentValue> documentValues;

    public interface DocumentAdd extends Default {
    }

    public interface DocumentEdit extends Default {
    }
}
