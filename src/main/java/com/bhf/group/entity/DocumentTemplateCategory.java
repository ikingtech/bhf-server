package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@Data
@TableName("document_template_category")
public class DocumentTemplateCategory implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    @NotBlank(message = "id不能为空", groups = {DocumentTemplateCategoryEdit.class})
    private String id;
    @NotBlank(message = "数据分类id不能为空", groups = {DocumentTemplateCategoryAdd.class, DocumentTemplateCategoryEdit.class})
    private String documentCategoryId;
    @NotBlank(message = "名称不能为空", groups = {DocumentTemplateCategoryAdd.class, DocumentTemplateCategoryEdit.class})
    private String name;
    private Integer status = 0;
    private String description;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    public interface DocumentTemplateCategoryAdd extends Default {
    }

    public interface DocumentTemplateCategoryEdit extends Default {
    }
}
