package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@Getter
@Setter
@TableName("sys_dictionary")
public class SysDictionary implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    @NotBlank(message = "id不能为空", groups = {SysDictionaryEdit.class})
    private String id;
    @NotBlank(message = "名称不能为空", groups = {SysDictionaryAdd.class, SysDictionaryEdit.class})
    private String name;
    @NotBlank(message = "value值不能为空", groups = {SysDictionaryAdd.class, SysDictionaryEdit.class})
    private String value;
    private String parentId;
    private int grade;
    private int status = 0;
    private String description;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    public interface SysDictionaryAdd extends Default {

    }

    public interface SysDictionaryEdit extends Default {

    }
}
