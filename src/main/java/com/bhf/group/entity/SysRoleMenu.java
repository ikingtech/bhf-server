package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@Data
@TableName("sys_role_menu")
public class SysRoleMenu implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String roleId;
    private String menuId;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
}
