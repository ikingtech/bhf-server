package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author pwq
 */
@Data
@TableName("sys_stop_word")
public class SysStopWord implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String word;
}
