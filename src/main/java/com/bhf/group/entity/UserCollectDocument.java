package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@Data
@TableName("user_collect_document")
public class UserCollectDocument implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String userId;
    private String documentId;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    private String authorId;
}
