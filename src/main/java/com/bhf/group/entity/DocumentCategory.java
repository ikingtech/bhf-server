package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
@Data
@TableName("document_category")
public class DocumentCategory implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    @NotBlank(message = "id不能为空", groups = {DocumentCategoryEdit.class})
    private String id;
    private String parentId = "0";
    @NotBlank(message = "名称不能为空", groups = {DocumentCategoryEdit.class, DocumentCategoryAdd.class})
    private String name;
    private Integer sort;
    private String icon;
    private Integer nums;
    private Integer status = 0;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(exist = false)
    private List<DocumentCategory> children;

    @TableField(exist = false)
    private List<Map<String, Object>> documents;

    public interface DocumentCategoryAdd extends Default {
    }

    public interface DocumentCategoryEdit extends Default {
    }

}
