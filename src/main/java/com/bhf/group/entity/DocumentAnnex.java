package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@Data
@TableName("document_annex")
public class DocumentAnnex implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String documentId;
    private String annexUrl;
    private String name;
    private String type;
    private Integer sort;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    private Integer nums;
}
