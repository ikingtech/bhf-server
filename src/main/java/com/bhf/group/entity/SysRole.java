package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@TableName("sys_role")
@Getter
@Setter
public class SysRole implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    @NotBlank(message = "id不能为空", groups = {RoleEdit.class})
    private String id;
    @NotBlank(message = "中文名称不能为空", groups = {RoleAdd.class, RoleEdit.class})
    private String name;
    @NotBlank(message = "英文名称不能为空", groups = {RoleAdd.class, RoleEdit.class})
    private String englishName;
    @Builder.Default
    private int status = 0;
    private String description;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    public interface RoleAdd extends Default {

    }

    public interface RoleEdit extends Default {

    }
}
