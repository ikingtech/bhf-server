package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @author pwq
 */
@Data
@TableName("document_template_options")
public class DocumentTemplateOptions implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String label;
    private String documentTemplateId;
    private String value;
}
