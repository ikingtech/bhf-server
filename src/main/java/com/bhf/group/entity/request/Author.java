package com.bhf.group.entity.request;

import lombok.Data;

/**
 * @author pwq
 */
@Data
public class Author {

    String author;
    int type;
    String searchType;
}
