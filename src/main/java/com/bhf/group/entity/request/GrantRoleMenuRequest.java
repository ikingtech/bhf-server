package com.bhf.group.entity.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * @author pwq
 */
@Data
public class GrantRoleMenuRequest implements Serializable {

    @NotBlank(message = "roleId不能为空")
    private String roleId;
    private String[] menuIds;
}
