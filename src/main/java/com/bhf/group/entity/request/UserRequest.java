package com.bhf.group.entity.request;

import com.bhf.group.core.Page;
import lombok.Getter;
import lombok.Setter;

/**
 * @author pwq
 */
@Getter
@Setter
public class UserRequest extends Page {

    private String username;
    private String mobile;
    private String department;
    private Integer sex;
}
