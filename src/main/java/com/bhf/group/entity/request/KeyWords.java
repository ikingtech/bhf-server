package com.bhf.group.entity.request;

import lombok.Data;

/**
 * @author pwq
 */
@Data
public class KeyWords {
    String word;
    int type;
    String searchType;
}
