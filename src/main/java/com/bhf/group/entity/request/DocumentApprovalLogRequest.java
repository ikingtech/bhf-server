package com.bhf.group.entity.request;

import com.bhf.group.core.Page;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author pwq
 */
@Getter
@Setter
public class DocumentApprovalLogRequest extends Page {

    @NotBlank(message = "documentId不能为空")
    private String documentId;
}
