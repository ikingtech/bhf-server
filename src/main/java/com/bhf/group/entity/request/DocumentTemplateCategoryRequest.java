package com.bhf.group.entity.request;

import com.bhf.group.core.Page;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author pwq
 */
@Getter
@Setter
public class DocumentTemplateCategoryRequest extends Page implements Serializable {

    private String name;
    private String description;
    private String documentCategoryId;
}
