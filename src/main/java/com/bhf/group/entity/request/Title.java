package com.bhf.group.entity.request;

import lombok.Data;

/**
 * @author pwq
 */
@Data
public class Title {
    String title;
    int type;
}
