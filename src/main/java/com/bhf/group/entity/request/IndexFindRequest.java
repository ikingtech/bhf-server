package com.bhf.group.entity.request;

import com.bhf.group.core.Page;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author pwq
 */

@Getter
@Setter
public class IndexFindRequest extends Page implements Serializable {

    private Title title;
    private Author author;
    private KeyWords keyWords;
    private String startTime;
    private String endTime;
    private String documentCategoryId;
    @Pattern(regexp = "^publish_time|collects|likes$", message = "只能publish_time或collects或likes")
    private String orderName;
    @NotBlank(message = "排序不能为空")
    @Pattern(regexp = "^desc|asc$", message = "desc或asc")
    private String orderType;
}

