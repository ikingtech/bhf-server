package com.bhf.group.entity.request;

import com.bhf.group.core.Page;
import lombok.Getter;
import lombok.Setter;

/**
 * @author pwq
 */
@Getter
@Setter
public class NewsRequest extends Page {

    private String title;
}
