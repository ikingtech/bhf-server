package com.bhf.group.entity.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author pwq
 */
@Getter
@Setter
public class ManagementDataRequest implements Serializable {
    @NotBlank(message = "approvalStatus不能为空")
    @Pattern(regexp = "^[2-3]$", message = "只能是数字2,3")
    private String approvalStatus;
    @NotBlank(message = "documentId不能为空")
    private String documentId;
    private String suggest;
    private String userId;
}
