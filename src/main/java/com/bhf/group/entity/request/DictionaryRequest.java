package com.bhf.group.entity.request;

import com.bhf.group.core.Page;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * @author pwq
 */
@Getter
@Setter
public class DictionaryRequest extends Page {

    private String name;
    private String value;
    private String description;
    @Builder.Default
    private String parentId = "0";
}
