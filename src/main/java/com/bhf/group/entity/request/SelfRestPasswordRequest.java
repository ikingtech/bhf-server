package com.bhf.group.entity.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author pwq
 */
@Data
public class SelfRestPasswordRequest implements Serializable {

    @Pattern(regexp = "^(?=.*[0-9].*)(?=.*[A-Z].*)(?=.*[a-z].*).{8,16}$", message = "必须包含大小写字母和数字并长度在8-16")
    @NotBlank(message = "密码不能为空")
    private String password;
    @NotBlank(message = "旧密码不能为空")
    private String oldPassword;
}
