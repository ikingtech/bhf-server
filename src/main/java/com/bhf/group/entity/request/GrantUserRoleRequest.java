package com.bhf.group.entity.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author pwq
 */
@Data
public class GrantUserRoleRequest implements Serializable {
    @NotEmpty(message = "roleId不能为空")
    @Size(message = "至少选择一种角色")
    private String[] roleIds;
    @NotBlank(message = "userIds不能为空")
    private String userId;
}
