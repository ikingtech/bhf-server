package com.bhf.group.entity.request;

import com.bhf.group.core.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author pwq
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class RoleRequest extends Page {
    private String name;
    private String englishName;
}
