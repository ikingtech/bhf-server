package com.bhf.group.entity.request;

import com.bhf.group.core.Page;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author pwq
 */
@Getter
@Setter
public class DocumentRequest extends Page implements Serializable {

    private String title;
    private String level;
    private String documentCategoryId;
    private String keyWords;
    private String userId;
    private Integer approvalStatus;
}
