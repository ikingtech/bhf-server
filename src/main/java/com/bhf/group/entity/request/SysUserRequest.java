package com.bhf.group.entity.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;

/**
 * @author pwq
 */
@Data
public class SysUserRequest implements Serializable {

    @NotBlank(message = "用户名不能为空")
    @Pattern(regexp = "^(?=.*?[\\u4E00-\\u9FA5])[\\d\\u4E00-\\u9FA5]{3,8}$", message = "至少3字符,包含汉字和数字,最大8个")
    private String username;
    @NotNull(message = "性别不能为空")
    @Pattern(regexp = "^[1-3]$", message = "只能是数字1,2,3")
    private String sex;
    @NotBlank(message = "单位不能为空")
    private String unit;
    private String department;
    @NotBlank(message = "头像不能为空")
    private String avatar;
    private String signature;
    private Integer certificateType;
    private String certificate;
    private String telephone;
}
