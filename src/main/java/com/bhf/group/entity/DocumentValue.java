package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author pwq
 */
@Data
@TableName("document_value")
public class DocumentValue implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String documentId;
    private String type;
    private String value;
    private String name;
    private String required = "false";
    private Integer sort = 0;
    private Integer custom;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField(exist = false)
    private List<DocumentValueOptions> valueOptions;
}
