package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@Data
@TableName("document_approval_log")
public class DocumentApprovalLog implements Serializable {
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String documentId;
    private String userId;
    private String suggest;
    private Integer approvalStatus;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

}
