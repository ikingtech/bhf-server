package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@Data
@TableName("sys_news")
public class SysNews implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    @NotBlank(message = "id不能为空", groups = {SysNewsEdit.class})
    private String id;
    @NotBlank(message = "标题不能为空", groups = {SysNewsAdd.class, SysNewsEdit.class})
    private String title;
    @NotBlank(message = "内容不能为空", groups = {SysNewsAdd.class, SysNewsEdit.class})
    private String content;
    private String createUser;
    private Integer status;
    private Integer type;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    private int previews;
    private LocalDateTime publishTime;

    public interface SysNewsAdd extends Default {

    }

    public interface SysNewsEdit extends Default {

    }
}
