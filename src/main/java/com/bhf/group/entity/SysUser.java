package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@Getter
@Setter
@TableName("sys_user")
public class SysUser implements Serializable {

    private static final long serialVersionUID = -2157054695038243027L;
    @NotBlank(message = "id不能为空", groups = {SysUserEdit.class})
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    @NotBlank(message = "用户名不能为空", groups = {SysUserAdd.class})
    @Pattern(regexp = "^(?=.*?[\\u4E00-\\u9FA5])[\\d\\u4E00-\\u9FA5]{3,8}$", message = "至少3字符,包含汉字和数字,最大8个", groups = {SysUserAdd.class})
    private String username;
    @Pattern(regexp = "^(?=.*[0-9].*)(?=.*[A-Z].*)(?=.*[a-z].*).{8,16}$", message = "必须包含大小写字母和数字并长度在8-16", groups = {SysUserAdd.class})
    @NotBlank(message = "密码不能为空", groups = {SysUserAdd.class})
    private String password;
    @Builder.Default
    @Pattern(regexp = "^[1-3]$", message = "只能是数字1，2，3")
    @NotBlank(message = "性别不能为空", groups = {SysUserAdd.class})
    private String sex="1";
    private String signature;
    private String department;
    @NotBlank(message = "单位不能为空", groups = {SysUserAdd.class})
    private String unit;
    @NotBlank(message = "头像不能为空", groups = {SysUserAdd.class})
    private String avatar;
    @Pattern(regexp = "^[0-1]$", message = "只能是数字0,1")
    @Builder.Default
    private String status = "0";
    @Builder.Default
    private Integer isFreeze = 0;
    private String userLevel;
    private Integer certificateType;
    private String certificate;
    private String telephone;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    public interface SysUserAdd extends Default {

    }

    public interface SysUserEdit extends Default {

    }
}
