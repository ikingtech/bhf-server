package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@TableName("user_operation_log")
@Getter
@Setter
@Builder
public class UserOperationLog implements Serializable {
    private static final long serialVersionUID = 110341046436277349L;

    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    private String userId;
    private String operation;
    private String methodName;
    private String ip;
    private String resultInfo;
    private String method;
    private String pathUrl;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    private Integer timeConsuming;
}
