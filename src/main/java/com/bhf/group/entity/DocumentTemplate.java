package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author pwq
 */
@Data
@TableName("document_template")
public class DocumentTemplate implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    @NotBlank(message = "id不能为空", groups = {DocumentTemplateEdit.class})
    private String id;
    @NotBlank(message = "名称不能为空", groups = {DocumentTemplateEdit.class, DocumentTemplateAdd.class})
    private String name;
    @NotBlank(message = "分类ID不能为空", groups = {DocumentTemplateEdit.class, DocumentTemplateAdd.class})
    private String categoryId;
    @NotBlank(message = "类型不能为空", groups = {DocumentTemplateEdit.class, DocumentTemplateAdd.class})
    private String type;
    @NotNull(message = "排序不能为空")
    private Integer sort = 0;
    @NotBlank(message = "不能为空", groups = {DocumentTemplateEdit.class, DocumentTemplateAdd.class})
    private String required = "false";
    private Integer status = 0;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    //private String model;

    @TableField(exist = false)
    private List<DocumentTemplateOptions> valueData;

    public interface DocumentTemplateAdd extends Default {
    }

    public interface DocumentTemplateEdit extends Default {
    }
}
