package com.bhf.group.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.groups.Default;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author pwq
 */
@Getter
@Setter
@TableName("sys_menu")
public class SysMenu implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    @NotBlank(message = "id不能为空", groups = {MenuEdit.class})
    private String id;
    @Builder.Default
    private String parentId = "0";
    @NotBlank(message = "中文名称不能为空", groups = {MenuAdd.class, MenuEdit.class})
    private String name;
    @NotBlank(message = "英文名称不能为空", groups = {MenuAdd.class, MenuEdit.class})
    private String englishName;
    @NotNull(message = "按钮类型不能为空", groups = {MenuAdd.class, MenuEdit.class})
    @Pattern(regexp = "^[0-1]$", message = "只能是数字0,1", groups = {MenuAdd.class, MenuEdit.class})
    private String menuType;
    @Builder.Default
    private int status = 0;
    private String path;
    private String component;
    private String icon;
    private int sorts;
    private String description;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableField(exist = false)
    private List<SysMenu> children;

    public interface MenuAdd extends Default {

    }

    public interface MenuEdit extends Default {

    }
}
