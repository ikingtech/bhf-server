package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.SysMenu;
import com.bhf.group.entity.SysUser;
import com.bhf.group.entity.request.UserRequest;

import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
public interface SysUserService extends IService<SysUser> {
    List<SysMenu> getUserAuthority(String id);

    Page<SysUser> getUserList(UserRequest request);

    Page<Map<String, String>> getUserCollectsList(String id, com.bhf.group.core.Page page);

    Page<Map<String, String>> getUserPreviewsList(String id, com.bhf.group.core.Page page);
}
