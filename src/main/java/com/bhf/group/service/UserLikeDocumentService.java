package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.UserLikeDocument;

/**
 * @author pwq
 */
public interface UserLikeDocumentService extends IService<UserLikeDocument> {
}
