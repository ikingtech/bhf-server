package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.DocumentTemplateCategory;

/**
 * @author pwq
 */
public interface DocumentTemplateCategoryService extends IService<DocumentTemplateCategory> {
}
