package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.DocumentValueOptions;

/**
 * @author pwq
 */
public interface DocumentValueOptionsService extends IService<DocumentValueOptions> {
}
