package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.SysStopWord;

/**
 * @author pwq
 */
public interface SysStopWordService extends IService<SysStopWord> {
}
