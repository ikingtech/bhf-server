package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.DocumentTemplate;

import java.util.List;

/**
 * @author pwq
 */
public interface DocumentTemplateService extends IService<DocumentTemplate> {
    boolean addDocumentTemplate(List<DocumentTemplate> template, String categoryId);

    List<DocumentTemplate> getDocumentTemplate(String categoryId);
}
