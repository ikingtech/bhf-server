package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.SysExtendWord;

/**
 * @author pwq
 */
public interface SysExtendWordService extends IService<SysExtendWord> {
}
