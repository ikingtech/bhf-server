package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.SysDictionary;
import com.bhf.group.entity.request.DictionaryRequest;

/**
 * @author pwq
 */
public interface SysDictionaryService extends IService<SysDictionary> {
    Page<SysDictionary> getDictionaryList(DictionaryRequest request);
}
