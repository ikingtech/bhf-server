package com.bhf.group.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.UserOperationLog;
import com.bhf.group.entity.request.UserLogsRequest;

import java.util.Map;

/**
 * @author pwq
 */
public interface UserOperationLogService extends IService<UserOperationLog> {
    IPage<Map<String, Object>> getUserOperationLogs(UserLogsRequest request);
}
