package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.SysMenu;
import com.bhf.group.entity.SysRole;

import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
public interface SysRoleService extends IService<SysRole> {
    List<SysMenu> getRoleAuthority(String roleId);

    List<Map<String, Object>> getUserRole(String id);
}
