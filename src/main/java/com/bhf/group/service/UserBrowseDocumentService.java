package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.UserBrowseDocument;

/**
 * @author pwq
 */
public interface UserBrowseDocumentService extends IService<UserBrowseDocument> {
}
