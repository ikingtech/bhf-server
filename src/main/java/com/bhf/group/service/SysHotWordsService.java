package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.SysHotWords;

/**
 * @author pwq
 */
public interface SysHotWordsService extends IService<SysHotWords>{
    void updateNums(String id);
}
