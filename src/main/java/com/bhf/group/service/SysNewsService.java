package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.core.Page;
import com.bhf.group.entity.SysNews;

import java.util.Map;

/**
 * @author pwq
 */
public interface SysNewsService extends IService<SysNews> {
    Map<String, Object> getNewsDetails(String id);

    com.baomidou.mybatisplus.extension.plugins.pagination.Page<Map<String, Object>> getNewsList(Page page);
}
