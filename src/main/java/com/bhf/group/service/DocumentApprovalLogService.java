package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.DocumentApprovalLog;
import com.bhf.group.entity.request.ManagementDataRequest;

/**
 * @author pwq
 */
public interface DocumentApprovalLogService extends IService<DocumentApprovalLog> {
    Object saveAndDocumentApprovalStatus(ManagementDataRequest request);

    String getApprovalUserName(String id);
}
