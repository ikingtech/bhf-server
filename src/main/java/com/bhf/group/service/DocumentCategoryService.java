package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.DocumentCategory;

import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
public interface DocumentCategoryService extends IService<DocumentCategory> {
    List<DocumentCategory> getDocumentCategoryList(String parentId);

    List<DocumentCategory> getDocumentCategoryId(String documentCategoryId);

    Map<String, Object> getHome();

    List<String> getDocumentCategoryListById(String id);

    void subDocumentCategoryNums(List<String> collect);
}
