package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.UserCollectDocument;

/**
 * @author pwq
 */
public interface UserCollectDocumentService extends IService<UserCollectDocument> {
}
