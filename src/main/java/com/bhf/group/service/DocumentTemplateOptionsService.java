package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.DocumentTemplateOptions;

/**
 * @author pwq
 */
public interface DocumentTemplateOptionsService extends IService<DocumentTemplateOptions> {

}
