package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.SysRoleMenu;

/**
 * @author pwq
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {
}
