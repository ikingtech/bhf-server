package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.SysUserRole;

/**
 * @author pwq
 */
public interface SysUserRoleService extends IService<SysUserRole> {
}
