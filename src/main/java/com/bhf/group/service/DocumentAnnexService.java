package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.DocumentAnnex;

/**
 * @author pwq
 */
public interface DocumentAnnexService extends IService<DocumentAnnex> {
    Boolean download(String id);
}
