package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.DocumentValue;

/**
 * @author pwq
 */
public interface DocumentValueService extends IService<DocumentValue> {
}
