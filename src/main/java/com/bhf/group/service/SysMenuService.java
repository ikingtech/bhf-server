package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.SysMenu;

import java.util.List;

/**
 * @author pwq
 */
public interface SysMenuService extends IService<SysMenu> {
    List<SysMenu> getMenuListByParentId(String parentId);

    List<SysMenu> getMenuListByParentId(String parentId, List<String> longs);
}
