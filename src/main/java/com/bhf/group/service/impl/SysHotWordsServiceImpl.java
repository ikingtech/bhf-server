package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.SysHotWords;
import com.bhf.group.mapper.SysHotWordsMapper;
import com.bhf.group.service.SysHotWordsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysHotWordsServiceImpl extends ServiceImpl<SysHotWordsMapper, SysHotWords> implements SysHotWordsService {
    @Override
    public void updateNums(String id) {
        this.baseMapper.updateNums(id);
    }
}
