package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.SysExtendWord;
import com.bhf.group.mapper.SysExtendWordMapper;
import com.bhf.group.service.SysExtendWordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysExtendWordServiceImpl extends ServiceImpl<SysExtendWordMapper, SysExtendWord> implements SysExtendWordService {
}
