package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.DocumentValue;
import com.bhf.group.mapper.DocumentValueMapper;
import com.bhf.group.service.DocumentValueService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DocumentValueServiceImpl extends ServiceImpl<DocumentValueMapper, DocumentValue> implements DocumentValueService {
}
