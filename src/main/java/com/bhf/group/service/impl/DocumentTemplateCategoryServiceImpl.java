package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.DocumentTemplateCategory;
import com.bhf.group.mapper.DocumentTemplateCategoryMapper;
import com.bhf.group.service.DocumentTemplateCategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DocumentTemplateCategoryServiceImpl extends ServiceImpl<DocumentTemplateCategoryMapper, DocumentTemplateCategory> implements DocumentTemplateCategoryService {
}
