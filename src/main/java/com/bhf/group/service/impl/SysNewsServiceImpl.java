package com.bhf.group.service.impl;

import cn.hutool.core.convert.Convert;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.core.Page;
import com.bhf.group.entity.SysNews;
import com.bhf.group.mapper.SysNewsMapper;
import com.bhf.group.service.SysNewsService;
import com.bhf.group.utils.EntityUtil;
import com.bhf.group.utils.NumberUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysNewsServiceImpl extends ServiceImpl<SysNewsMapper, SysNews> implements SysNewsService {
    @Override
    public Map<String, Object> getNewsDetails(String id) {
        this.baseMapper.updateNewsPreviews(id);
        QueryWrapper<Map<String, Object>> wrapper = new QueryWrapper<>();
        wrapper.eq("sn.type", 1);
        wrapper.eq("sn.id", id);
        wrapper.eq("sn.status", 0);
        final Map<String, Object> objectMap = EntityUtil.beanToMap(this.baseMapper.selectOneById(wrapper));
        objectMap.put("previews", NumberUtil.getUnitOfNumber(Long.parseLong(String.valueOf(objectMap.get("previews")))));
        return objectMap;
    }

    @Override
    public com.baomidou.mybatisplus.extension.plugins.pagination.Page<Map<String, Object>> getNewsList(Page page) {
        QueryWrapper<Map<String, Object>> wrapper = new QueryWrapper<>();
        wrapper.eq("sn.type", 1);
        wrapper.eq("sn.status", 0);
        wrapper.orderByDesc("sn.publish_time");
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Map<String, Object>> newsList = this.baseMapper.getNewsList(new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>(page.getPages(), page.getSize()), wrapper);
        final List<Map<String, Object>> collect = newsList.getRecords().stream().peek(e -> {
            e.put("previews", NumberUtil.getUnitOfNumber(Long.parseLong(String.valueOf(e.get("previews")))));
            e.put("content", Convert.toStr(e.get("content")));
            e.put("id", Convert.toStr(e.get("id")));
        }).collect(Collectors.toList());
        newsList.setRecords(collect);
        return newsList;
    }
}
