package com.bhf.group.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.constants.Constants;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.entity.Document;
import com.bhf.group.entity.DocumentApprovalLog;
import com.bhf.group.entity.DocumentCategory;
import com.bhf.group.entity.request.ManagementDataRequest;
import com.bhf.group.mapper.DocumentApprovalLogMapper;
import com.bhf.group.service.DocumentApprovalLogService;
import com.bhf.group.service.DocumentCategoryService;
import com.bhf.group.service.DocumentService;
import com.bhf.group.utils.RestResultUtil;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DocumentApprovalLogServiceImpl extends ServiceImpl<DocumentApprovalLogMapper, DocumentApprovalLog> implements DocumentApprovalLogService {

    @Resource
    private DocumentService documentService;
    @Resource
    private DocumentCategoryService documentCategoryService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public Object saveAndDocumentApprovalStatus(ManagementDataRequest request) {
        final DocumentApprovalLog log = new DocumentApprovalLog();
        log.setUserId(request.getUserId());
        log.setDocumentId(request.getDocumentId());
        log.setApprovalStatus(Integer.valueOf(request.getApprovalStatus()));
        log.setSuggest(request.getSuggest());
        if ("3".equals(request.getApprovalStatus()) && StrUtil.isBlank(request.getSuggest())) {
            return RestResultUtil.buildResult(ResultCodeEnum.APPROVAL_SUGGEST_NOTNULL_ERROR);
        }
        if ("2".equals(request.getApprovalStatus()) && StrUtil.isBlank(request.getSuggest())) {
            log.setSuggest("审核通过");
        }
        final Document document = new Document();
        document.setId(request.getDocumentId());
        document.setApprovalStatus(Integer.valueOf(request.getApprovalStatus()));
        if ("2".equals(request.getApprovalStatus())) {
            final Document doc = documentService.getById(request.getDocumentId());
            List<DocumentCategory> ids = documentCategoryService.getDocumentCategoryId(doc.getDocumentCategoryId());
            final DocumentCategory category = ids.stream().filter(e -> Integer.parseInt(e.getParentId()) == 0).findFirst().orElse(null);
            final List<DocumentCategory> collect = ids.stream().peek(e -> e.setNums(e.getNums() + 1)).collect(Collectors.toList());
            documentCategoryService.updateBatchById(collect);
            document.setDocumentCategoryParentId(category.getId());
            document.setPublishTime(LocalDateTime.now());
            redisTemplate.delete(Constants.DATA_CATEGORY);
            redisTemplate.delete(Constants.DATA_CATEGORY_DETAILS);
        }
        documentService.updateById(document);
        return this.baseMapper.insert(log) > 0;
    }

    @Override
    public String getApprovalUserName(String id) {
        return this.baseMapper.getApprovalUserName(id);
    }
}
