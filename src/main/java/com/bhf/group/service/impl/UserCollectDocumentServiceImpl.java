package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.UserCollectDocument;
import com.bhf.group.mapper.UserCollectDocumentMapper;
import com.bhf.group.service.UserCollectDocumentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserCollectDocumentServiceImpl extends ServiceImpl<UserCollectDocumentMapper, UserCollectDocument> implements UserCollectDocumentService {
}
