package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.DocumentAnnex;
import com.bhf.group.mapper.DocumentAnnexMapper;
import com.bhf.group.service.DocumentAnnexService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DocumentAnnexServiceImpl extends ServiceImpl<DocumentAnnexMapper, DocumentAnnex> implements DocumentAnnexService {
    @Override
    public Boolean download(String id) {
        this.baseMapper.download(id);
        return true;
    }
}
