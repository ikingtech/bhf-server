package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.DocumentTemplateOptions;
import com.bhf.group.mapper.DocumentTemplateOptionsMapper;
import com.bhf.group.service.DocumentTemplateOptionsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DocumentTemplateOptionsServiceImpl extends ServiceImpl<DocumentTemplateOptionsMapper, DocumentTemplateOptions> implements DocumentTemplateOptionsService {
}
