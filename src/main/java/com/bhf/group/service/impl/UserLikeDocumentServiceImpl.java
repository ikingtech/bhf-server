package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.UserLikeDocument;
import com.bhf.group.mapper.UserLikeDocumentMapper;
import com.bhf.group.service.UserLikeDocumentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserLikeDocumentServiceImpl extends ServiceImpl<UserLikeDocumentMapper, UserLikeDocument> implements UserLikeDocumentService {
}
