package com.bhf.group.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.constants.UserLevelEnum;
import com.bhf.group.entity.*;
import com.bhf.group.entity.request.DocumentApprovalLogRequest;
import com.bhf.group.entity.request.DocumentRequest;
import com.bhf.group.entity.request.IndexFindRequest;
import com.bhf.group.mapper.DocumentApprovalLogMapper;
import com.bhf.group.mapper.DocumentMapper;
import com.bhf.group.service.*;
import com.bhf.group.utils.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DocumentServiceImpl extends ServiceImpl<DocumentMapper, Document> implements DocumentService {

    public static final String NOT = "NOT";
    public static final String AND = "AND";
    public static final String OR = "OR";

    @Resource
    private DocumentAnnexService documentAnnexService;
    @Resource
    private DocumentValueService documentValueService;
    @Resource
    private DocumentValueOptionsService documentValueOptionsService;
    @Resource
    private DocumentApprovalLogMapper documentApprovalLogMapper;
    @Resource
    private SysUserService sysUserService;
    @Resource
    private UserLikeDocumentService userLikeDocumentService;
    @Resource
    private UserCollectDocumentService userCollectDocumentService;
    @Resource
    private UserBrowseDocumentService userBrowseDocumentService;
    @Resource
    private DocumentCategoryService documentCategoryService;
    @Resource
    private TokenizerUtil tokenizerUtil;
    @Resource
    private DocumentApprovalLogService documentApprovalLogService;


    @Override
    public Boolean addDocument(Document document) {
        int count;
        if (StrUtil.isNotBlank(document.getId())) {
            count = 1;
        } else {
            count = this.baseMapper.insert(document);
        }
        if (count > 0) {
            if (Objects.nonNull(document.getDocumentAnnexList()) && document.getDocumentAnnexList().size() > 0) {
                List<DocumentAnnex> annexList = new ArrayList<>();
                for (DocumentAnnex documentAnnex : document.getDocumentAnnexList()) {
                    documentAnnex.setDocumentId(document.getId());
                    annexList.add(documentAnnex);
                }
                documentAnnexService.saveBatch(annexList);
            }
            if (Objects.nonNull(document.getDocumentValues()) && document.getDocumentValues().size() > 0) {
                List<DocumentValue> values = new ArrayList<>();
                for (DocumentValue documentValue : document.getDocumentValues()) {
                    documentValue.setDocumentId(document.getId());
                    values.add(documentValue);
                }
                documentValueService.saveBatch(values);
                List<DocumentValueOptions> options = new ArrayList<>();
                for (DocumentValue value : values) {
                    if (Objects.nonNull(value.getValueOptions()) && value.getValueOptions().size() > 0) {
                        for (DocumentValueOptions valueOption : value.getValueOptions()) {
                            valueOption.setDocumentValueId(value.getId());
                            options.add(valueOption);
                        }
                    }
                }
                documentValueOptionsService.saveBatch(options);
            }
        }
        if (document.getApprovalStatus() == 1) {
            documentApproval(document.getId(), document.getUserId());
        }
        return count > 0;
    }

    @Override
    public Boolean editDocument(Document document) {
        final int count = this.baseMapper.updateById(document);
        if (count > 0) {
            LambdaQueryWrapper<DocumentAnnex> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(DocumentAnnex::getDocumentId, document.getId());
            documentAnnexService.remove(wrapper);
            LambdaQueryWrapper<DocumentValue> documentValue = new LambdaQueryWrapper<>();
            documentValue.eq(DocumentValue::getDocumentId, document.getId());
            final List<DocumentValue> list = documentValueService.list(documentValue);
            final List<String> documentValueIds = list.stream().map(DocumentValue::getId).collect(Collectors.toList());
            LambdaQueryWrapper<DocumentValueOptions> optionsLambdaQueryWrapper = new LambdaQueryWrapper<>();
            optionsLambdaQueryWrapper.in(DocumentValueOptions::getDocumentValueId, documentValueIds);
            documentValueOptionsService.remove(optionsLambdaQueryWrapper);
            documentValueService.removeByIds(documentValueIds);
        }
        return addDocument(document);
    }

    @Override
    public Page<Document> getDocumentList(DocumentRequest request, List<Integer> status) {
        LambdaQueryWrapper<Document> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Document::getStatus, 0);
        wrapper.in(Document::getApprovalStatus, status);
        if (StrUtil.isNotBlank(request.getTitle())) {
            wrapper.like(Document::getTitle, request.getTitle());
        }
        if (StrUtil.isNotBlank(request.getLevel())) {
            wrapper.eq(Document::getLevel, request.getLevel());
        }
        if (StrUtil.isNotBlank(request.getDocumentCategoryId())) {
            wrapper.eq(Document::getDocumentCategoryId, request.getDocumentCategoryId());
        }
        if (StrUtil.isNotBlank(request.getKeyWords())) {
            wrapper.like(Document::getKeyWords, request.getKeyWords());
        }
        if (StrUtil.isNotBlank(request.getUserId())) {
            wrapper.eq(Document::getUserId, request.getUserId());
        }
        wrapper.orderByDesc(Document::getCreateTime);
        return this.baseMapper.getDocumentPageList(new Page<>(request.getPages(), request.getSize()), wrapper);
    }

    @Override
    public Boolean documentApproval(String id, String userId) {
        final Document document = new Document();
        document.setId(id);
        document.setApprovalStatus(1);
        DocumentApprovalLog log = new DocumentApprovalLog();
        log.setDocumentId(id);
        log.setSuggest("提交审核");
        log.setApprovalStatus(1);
        log.setUserId(userId);
        documentApprovalLogMapper.insert(log);
        return this.baseMapper.updateById(document) > 0;
    }

    @Override
    public Page<Map<String, Object>> getDocumentApprovalLogList(DocumentApprovalLogRequest request) {
        QueryWrapper<Map<String, Objects>> wrapper = new QueryWrapper<>();
        wrapper.eq("dal.document_id", request.getDocumentId()).orderByDesc("dal.create_time");
        return documentApprovalLogMapper.getDocumentApprovalLogList(new Page<>(request.getPages(), request.getSize()), wrapper);
    }

    @Override
    public Page<Map<String, Object>> findIndexDocumentData(IndexFindRequest request) {
        QueryWrapper<Map<String, Object>> wrapper = new QueryWrapper<>();
        wrapper.eq("doc.status", 0).eq("doc.approval_status", 2);
        if (Objects.nonNull(request.getTitle()) && StrUtil.isNotBlank(request.getTitle().getTitle())) {
            if (request.getTitle().getType() == 0) {
                wrapper.like("doc.title", request.getTitle().getTitle());
            } else {
                wrapper.eq("doc.title", request.getTitle().getTitle());
            }
        }
        if (Objects.nonNull(request.getAuthor()) && StrUtil.isNotBlank(request.getAuthor().getAuthor())) {
            if (request.getAuthor().getSearchType().equalsIgnoreCase(OR)) {
                if (request.getAuthor().getType() == 0) {
                    wrapper.or(e -> e.like("su.username", request.getAuthor().getAuthor()));
                } else {
                    wrapper.or(e -> e.eq("su.username", request.getAuthor().getAuthor()));
                }
            }
            if (request.getAuthor().getSearchType().equalsIgnoreCase(AND)) {
                if (request.getAuthor().getType() == 0) {
                    wrapper.and(e -> e.like("su.username", request.getAuthor().getAuthor()));
                } else {
                    wrapper.and(e -> e.eq("su.username", request.getAuthor().getAuthor()));
                }
            }
            if (request.getAuthor().getSearchType().equalsIgnoreCase(NOT)) {
                if (request.getAuthor().getType() == 0) {
                    wrapper.not(e -> e.like("su.username", request.getAuthor().getAuthor()));
                } else {
                    wrapper.not(e -> e.eq("su.username", request.getAuthor().getAuthor()));
                }
            }

        }
        if (Objects.nonNull(request.getKeyWords()) && StrUtil.isNotBlank(request.getKeyWords().getWord())) {
            if (request.getKeyWords().getSearchType().equalsIgnoreCase(NOT)) {
                if (request.getKeyWords().getType() == 0) {
                    wrapper.not(e -> e.like("doc.key_words", request.getKeyWords().getWord()));
                } else {
                    wrapper.not(e -> e.eq("doc.key_words", request.getKeyWords().getWord()));
                }
            }
            if (request.getKeyWords().getSearchType().equalsIgnoreCase(OR)) {
                if (request.getKeyWords().getType() == 0) {
                    wrapper.or(e -> e.like("doc.key_words", request.getKeyWords().getWord()));
                } else {
                    wrapper.or(e -> e.eq("doc.key_words", request.getKeyWords().getWord()));
                }
            }
            if (request.getKeyWords().getSearchType().equalsIgnoreCase(AND)) {
                if (request.getKeyWords().getType() == 0) {
                    wrapper.and(e -> e.like("doc.key_words", request.getKeyWords().getWord()));
                } else {
                    wrapper.and(e -> e.eq("doc.key_words", request.getKeyWords().getWord()));
                }
            }
        }
        if (StrUtil.isNotBlank(request.getDocumentCategoryId())) {
            wrapper.in("doc.document_category_id", documentCategoryService.getDocumentCategoryListById(request.getDocumentCategoryId()));
        }
        if (StrUtil.isNotBlank(request.getStartTime())) {
            wrapper.ge("to_date(publish_time,'yyyy-MM-dd')", DateUtil.format(DateUtil.parse(request.getStartTime()), "yyyy-MM-dd"));
        }
        if (StrUtil.isNotBlank(request.getEndTime())) {
            wrapper.le("to_date(publish_time,'yyyy-MM-dd')", DateUtil.format(DateUtil.parse(request.getEndTime()), "yyyy-MM-dd"));
        }
        wrapper.orderBy(true, !"desc".equals(request.getOrderType()), "doc." + request.getOrderName());

        final Page<Map<String, Object>> indexDocumentData = this.baseMapper.findIndexDocumentData(new Page<>(request.getPages(), request.getSize()), wrapper);
        final List<Map<String, Object>> records = indexDocumentData.getRecords();
        final List<Map<String, Object>> collect = records.stream().peek(r -> {
            r.put("likes", NumberUtil.getUnitOfNumber(Long.parseLong(String.valueOf(r.get("likes")))));
            r.put("previews", NumberUtil.getUnitOfNumber(Long.parseLong(String.valueOf(r.get("previews")))));
            r.put("collects", NumberUtil.getUnitOfNumber(Long.parseLong(String.valueOf(r.get("collects")))));
        }).collect(Collectors.toList());
        indexDocumentData.setRecords(collect);
        return indexDocumentData;
    }

    @Override
    public RestResult<Object> getDocumentDetailsById(String id, String userId) throws IOException {
        this.baseMapper.updateDocumentPreviews(id, 1);
        Map<String, Object> result = new HashMap<>(3);
        LambdaQueryWrapper<Document> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Document::getApprovalStatus, 2).eq(Document::getId, id);

        Document document = this.baseMapper.getDocumentDetailsById(wrapper);
        if (Objects.isNull(document)) {
            return RestResultUtil.buildResult(ResultCodeEnum.DOCUMENT_EXIST_ERROR);
        }
        final Map<String, Object> objectMap = EntityUtil.beanToMap(document);
        String username = documentApprovalLogService.getApprovalUserName(id);
        objectMap.put("approvalUser",username);
        objectMap.put("previews", NumberUtil.getUnitOfNumber(document.getPreviews()));
        objectMap.put("collects", NumberUtil.getUnitOfNumber(document.getCollects()));
        objectMap.put("likes", NumberUtil.getUnitOfNumber(document.getLikes()));
        final SysUser user = sysUserService.getById(document.getUserId());
        Map<String, Object> userMap = new HashMap<>(3);
        userMap.put("username", user.getUsername());
        userMap.put("signature", user.getSignature());
        userMap.put("sex", user.getSex());
        userMap.put("avatar", user.getAvatar());
        LambdaQueryWrapper<Document> authorWrapper = new LambdaQueryWrapper<>();
        authorWrapper.eq(Document::getApprovalStatus, 2).eq(Document::getUserId, user.getId()).and(e -> e.ne(Document::getId, id)).orderByDesc(Document::getPublishTime);
        final Page<Document> records = this.baseMapper.selectPage(new Page<>(1, 10), authorWrapper);
        LambdaQueryWrapper<UserCollectDocument> collect = new LambdaQueryWrapper<>();
        collect.eq(UserCollectDocument::getAuthorId, document.getUserId());
        userMap.put("collects", NumberUtil.getUnitOfNumber(userCollectDocumentService.count(collect)));
        userMap.put("documents", NumberUtil.getUnitOfNumber(records.getTotal() + 1));
        final List<Map<String, Object>> collect1 = records.getRecords().stream().map(c -> {
            final Map<String, Object> objectMap1 = EntityUtil.beanToMap(c);
            objectMap1.put("previews", NumberUtil.getUnitOfNumber(c.getPreviews()));
            objectMap1.put("collects", NumberUtil.getUnitOfNumber(c.getCollects()));
            objectMap1.put("likes", NumberUtil.getUnitOfNumber(c.getLikes()));
            return objectMap1;
        }).collect(Collectors.toList());
        userMap.put("relevant", collect1);
        if (StrUtil.isNotBlank(userId)) {
            LambdaQueryWrapper<UserCollectDocument> collectDocument = new LambdaQueryWrapper<>();
            collectDocument.eq(UserCollectDocument::getDocumentId, id).eq(UserCollectDocument::getUserId, userId);
            final int count = userCollectDocumentService.count(collectDocument);
            result.put("collected", count > 0);
            LambdaQueryWrapper<UserLikeDocument> likeDocumentLambdaQueryWrapper = new LambdaQueryWrapper<>();
            likeDocumentLambdaQueryWrapper.eq(UserLikeDocument::getDocumentId, id).eq(UserLikeDocument::getUserId, userId);
            final int count1 = userLikeDocumentService.count(likeDocumentLambdaQueryWrapper);
            result.put("liked", count1 > 0);
            LambdaQueryWrapper<UserBrowseDocument> browseDocument = new LambdaQueryWrapper<>();
            browseDocument.eq(UserBrowseDocument::getUserId, userId).eq(UserBrowseDocument::getDocumentId, id);
            final UserBrowseDocument one = userBrowseDocumentService.getOne(browseDocument);
            if (Objects.nonNull(one)) {
                userBrowseDocumentService.removeById(one.getId());
            }
            UserBrowseDocument browse = new UserBrowseDocument();
            browse.setDocumentId(id);
            browse.setAuthorId(document.getUserId());
            browse.setUserId(userId);
            userBrowseDocumentService.save(browse);
        } else {
            result.put("collected", false);
            result.put("liked", false);
        }
        if (UserLevelEnum.getGrade(document.getLevel()) > 0) {
            if (StrUtil.isBlank(userId)) {
                document.setDocumentValues(null);
                document.setDocumentAnnexList(null);
                result.put("level", ResultCodeEnum.USER_LEVEL_ERROR.getMsg());
            } else {
                final SysUser sysUser = sysUserService.getById(userId);
                if (UserLevelEnum.getGrade(document.getLevel()) > UserLevelEnum.getGrade(sysUser.getUserLevel())) {
                    document.setDocumentValues(null);
                    document.setDocumentAnnexList(null);
                    result.put("level", ResultCodeEnum.USER_LEVEL_ERROR.getMsg());
                } else {
                    result.put("level", 0);
                }
            }
        } else {
            result.put("level", 0);
        }
        LambdaQueryWrapper<UserBrowseDocument> browseDocumentLambdaQueryWrapper = new LambdaQueryWrapper<>();
        browseDocumentLambdaQueryWrapper.eq(UserBrowseDocument::getAuthorId, document.getUserId());
        userMap.put("browses", NumberUtil.getUnitOfNumber(userBrowseDocumentService.count(browseDocumentLambdaQueryWrapper)));
        result.put("author", userMap);
        result.put("document", objectMap);
        result.put("likeness",likeness(document));
        return RestResultUtil.success(result);
    }

    private Object likeness(Document document) throws IOException {
       final List<String> list = tokenizerUtil.smartChineseAnalyzer(document.getTitle());
        final String[] split = document.getKeyWords().split(",");
        if (Objects.nonNull(split) && split.length>0){
            list.addAll(Arrays.asList(split));
        }
        List<String> myList = list.stream().distinct().collect(Collectors.toList());
        if (Objects.nonNull(myList) && myList.size()>0){
            QueryWrapper<Map<String, Object>> wrapper = new QueryWrapper<>();
            wrapper.eq("doc.status", 0).eq("doc.approval_status", 2).ne("doc.id",document.getId());
            StringBuilder sql = new StringBuilder();
            for (int i = 0; i < myList.size(); i++) {
                if (i==0){
                    if (i==myList.size()-1){
                        sql.append("(doc.title LIKE '%").append(myList.get(i)).append("%')");
                    }else {
                        sql.append("(doc.title LIKE '%").append(myList.get(i)).append("%'");
                    }
                }else if (i==myList.size()-1){
                    sql.append(" or doc.title LIKE '%").append(myList.get(i)).append("%')");
                }else {
                    sql.append(" or doc.title LIKE '%").append(myList.get(i)).append("%'");
                }
            }
            if (StrUtil.isNotBlank(sql.toString())){
                wrapper.apply(sql.toString(),myList);
            }
            wrapper.orderByDesc("doc.publish_time");
            final Page<Map<String, Object>> indexDocumentData = this.baseMapper.findIndexDocumentData(new Page<>(1, 10), wrapper);
            return indexDocumentData.getRecords();
        }
        return null;
    }

    @Override
    public boolean likeDocument(String id, String userId) {
        LambdaQueryWrapper<UserLikeDocument> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserLikeDocument::getDocumentId, id).eq(UserLikeDocument::getUserId, userId);
        UserLikeDocument like = userLikeDocumentService.getOne(wrapper);
        if (Objects.nonNull(like)) {
            this.baseMapper.updateDocumentLike(id, -1);
            return userLikeDocumentService.removeById(like.getId());
        } else {
            UserLikeDocument userLikeDocument = new UserLikeDocument();
            userLikeDocument.setUserId(userId);
            userLikeDocument.setDocumentId(id);
            this.baseMapper.updateDocumentLike(id, 1);
            return userLikeDocumentService.save(userLikeDocument);
        }
    }

    @Override
    public boolean collectDocument(String id, String userId) {
        LambdaQueryWrapper<UserCollectDocument> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(UserCollectDocument::getUserId, userId).eq(UserCollectDocument::getDocumentId, id);
        final UserCollectDocument collect = userCollectDocumentService.getOne(wrapper);
        if (Objects.nonNull(collect)) {
            this.baseMapper.updateDocumentCollect(id, -1);
            return userCollectDocumentService.removeById(collect.getId());
        } else {
            final Document document = this.baseMapper.selectById(id);
            UserCollectDocument collectDocument = new UserCollectDocument();
            collectDocument.setUserId(userId);
            collectDocument.setDocumentId(id);
            collectDocument.setAuthorId(document.getUserId());
            this.baseMapper.updateDocumentCollect(id, 1);
            return userCollectDocumentService.save(collectDocument);
        }
    }
}
