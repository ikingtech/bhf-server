package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.SysMenu;
import com.bhf.group.mapper.SysMenuMapper;
import com.bhf.group.service.SysMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {
    @Override
    public List<SysMenu> getMenuListByParentId(String parentId) {
        LambdaQueryWrapper<SysMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysMenu::getParentId, parentId).orderByAsc(SysMenu::getSorts);
        final List<SysMenu> list = this.baseMapper.selectList(wrapper);
        if (list.size() > 0 && Objects.nonNull(list)) {
            for (SysMenu menu : list) {
                menu.setChildren(getChildrenMenu(menu.getId(), null));
            }
        }
        return list;
    }

    @Override
    public List<SysMenu> getMenuListByParentId(String parentId, List<String> longs) {
        LambdaQueryWrapper<SysMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysMenu::getParentId, parentId).orderByAsc(SysMenu::getSorts);
        wrapper.in(SysMenu::getId, longs);
        final List<SysMenu> list = this.baseMapper.selectList(wrapper);
        if (list.size() > 0 && Objects.nonNull(list)) {
            for (SysMenu menu : list) {
                menu.setChildren(getChildrenMenu(menu.getId(), longs));
            }
        }
        return list;
    }

    private List<SysMenu> getChildrenMenu(String parentId, List<String> longs) {
        List<SysMenu> newMenuList = new ArrayList<>();
        LambdaQueryWrapper<SysMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysMenu::getParentId, parentId).orderByAsc(SysMenu::getSorts);
        if (Objects.nonNull(longs)) {
            wrapper.in(SysMenu::getId, longs);
        }
        final List<SysMenu> list = this.baseMapper.selectList(wrapper);
        if (list.size() > 0 && Objects.nonNull(list)) {
            for (SysMenu menu : list) {
                menu.setChildren(getChildrenMenu(menu.getId(), longs));
                newMenuList.add(menu);
            }
        }
        return newMenuList;
    }
}
