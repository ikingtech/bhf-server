package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.UserBrowseDocument;
import com.bhf.group.mapper.UserBrowseDocumentMapper;
import com.bhf.group.service.UserBrowseDocumentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserBrowseDocumentServiceImpl extends ServiceImpl<UserBrowseDocumentMapper, UserBrowseDocument> implements UserBrowseDocumentService {
}
