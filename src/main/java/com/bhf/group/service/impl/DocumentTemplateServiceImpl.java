package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.DocumentTemplate;
import com.bhf.group.entity.DocumentTemplateOptions;
import com.bhf.group.mapper.DocumentTemplateMapper;
import com.bhf.group.service.DocumentTemplateOptionsService;
import com.bhf.group.service.DocumentTemplateService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DocumentTemplateServiceImpl extends ServiceImpl<DocumentTemplateMapper, DocumentTemplate> implements DocumentTemplateService {

    @Resource
    private DocumentTemplateOptionsService documentTemplateOptionsService;

    @Override
    public boolean addDocumentTemplate(List<DocumentTemplate> template, String categoryId) {
        LambdaQueryWrapper<DocumentTemplate> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(DocumentTemplate::getCategoryId, categoryId);
        this.baseMapper.delete(wrapper);
        final boolean b = this.saveOrUpdateBatch(template);
        for (DocumentTemplate documentTemplate : template) {
            if (Objects.nonNull(documentTemplate.getValueData()) && documentTemplate.getValueData().size() > 0) {
                List<DocumentTemplateOptions> list = new ArrayList<>();
                for (DocumentTemplateOptions valueDatum : documentTemplate.getValueData()) {
                    DocumentTemplateOptions options = new DocumentTemplateOptions();
                    options.setLabel(valueDatum.getLabel());
                    options.setValue(valueDatum.getValue());
                    options.setDocumentTemplateId(documentTemplate.getId());
                    list.add(options);
                }
                documentTemplateOptionsService.saveOrUpdateBatch(list);
            }
        }
        return true;
    }

    @Override
    public List<DocumentTemplate> getDocumentTemplate(String categoryId) {
        return this.baseMapper.getDocumentTemplateList(categoryId);
    }
}
