package com.bhf.group.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.UserOperationLog;
import com.bhf.group.entity.request.UserLogsRequest;
import com.bhf.group.mapper.UserOperationLogMapper;
import com.bhf.group.service.UserOperationLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserOperationLogServiceImpl extends ServiceImpl<UserOperationLogMapper, UserOperationLog> implements UserOperationLogService {

    @Override
    public IPage<Map<String, Object>> getUserOperationLogs(UserLogsRequest request) {
        QueryWrapper<Map<String, Object>> wrapper = new QueryWrapper<>();
        if (StrUtil.isNotBlank(request.getUsername())) {
            wrapper.like("su.username", request.getUsername());
        }
        if (request.getUserId() != null) {
            wrapper.eq("upl.user_id", request.getUserId());
        }
        final IPage<Map<String, Object>> page = new Page<>(request.getPages(), request.getSize());
        wrapper.orderByDesc("upl.create_time");
        return this.baseMapper.getUserOperationLogs(page, wrapper);
    }
}
