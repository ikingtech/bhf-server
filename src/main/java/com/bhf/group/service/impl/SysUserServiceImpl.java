package com.bhf.group.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.SysMenu;
import com.bhf.group.entity.SysRoleMenu;
import com.bhf.group.entity.SysUser;
import com.bhf.group.entity.request.UserRequest;
import com.bhf.group.mapper.SysRoleMenuMapper;
import com.bhf.group.mapper.SysUserMapper;
import com.bhf.group.mapper.SysUserRoleMapper;
import com.bhf.group.security.entity.RoleInfo;
import com.bhf.group.service.SysMenuService;
import com.bhf.group.service.SysUserService;
import com.bhf.group.utils.NumberUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Resource
    private SysRoleMenuMapper sysRoleMenuMapper;
    @Resource
    private SysMenuService sysMenuService;
    @Resource
    private SysUserRoleMapper sysUserRoleMapper;

    @Override
    public List<SysMenu> getUserAuthority(String id) {
        final List<RoleInfo> roles = this.baseMapper.getRoles(id);
        final List<String> collect = roles.stream().map(RoleInfo::getId).collect(Collectors.toList());
        LambdaQueryWrapper<SysRoleMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(SysRoleMenu::getRoleId, collect);
        final List<SysRoleMenu> sysRoleMenus = sysRoleMenuMapper.selectList(wrapper);
        final List<String> longs = sysRoleMenus.stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
        if (Objects.nonNull(longs) && longs.size()>0){
            return sysMenuService.getMenuListByParentId("0", longs);
        }
        return null;
    }

    @Override
    public Page<SysUser> getUserList(UserRequest request) {
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.select(SysUser::getId,SysUser::getUsername,SysUser::getSex,SysUser::getSignature,SysUser::getDepartment,
                SysUser::getCertificateType,SysUser::getCertificate,SysUser::getTelephone,
                SysUser::getAvatar,SysUser::getUnit,SysUser::getIsFreeze,SysUser::getUserLevel,SysUser::getCreateTime);
        if (StrUtil.isNotBlank(request.getUsername())) {
            wrapper.like(SysUser::getUsername, request.getUsername());
        }
        if (StrUtil.isNotBlank(request.getDepartment())) {
            wrapper.like(SysUser::getDepartment, request.getDepartment());
        }
        if (Objects.nonNull(request.getSex())) {
            wrapper.eq(SysUser::getSex, request.getSex());
        }
        wrapper.orderByDesc(SysUser::getCreateTime);
        return this.baseMapper.selectPage(new Page<>(request.getPages(), request.getSize()),wrapper);
    }

    @Override
    public Page<Map<String, String>> getUserCollectsList(String id, com.bhf.group.core.Page page) {
        QueryWrapper<Map<String, String>> wrapper = new QueryWrapper<>();
        wrapper.eq("ucd.user_id", id).orderByDesc("ucd.create_time");
        final Page<Map<String, String>> userCollectsList = this.baseMapper.getUserCollectsList(new Page<>(page.getPages(), page.getSize()), wrapper);
        final List<Map<String, String>> maps = commonPreviewsAndCollectsResult(userCollectsList.getRecords());
        return userCollectsList.setRecords(maps);
    }

    @Override
    public Page<Map<String, String>> getUserPreviewsList(String id, com.bhf.group.core.Page page) {
        QueryWrapper<Map<String, String>> wrapper = new QueryWrapper<>();
        wrapper.eq("ubd.user_id", id).orderByDesc("ubd.create_time");
        final Page<Map<String, String>> userPreviewsList = this.baseMapper.getUserPreviewsList(new Page<>(page.getPages(), page.getSize()), wrapper);
        final List<Map<String, String>> maps = commonPreviewsAndCollectsResult(userPreviewsList.getRecords());
        return userPreviewsList.setRecords(maps);
    }

    private List<Map<String, String>> commonPreviewsAndCollectsResult(List<Map<String, String>> records) {
        return records.stream().peek(e -> {
            e.put("previews", NumberUtil.getUnitOfNumber(Long.parseLong(String.valueOf(e.get("previews")))));
            e.put("collects", NumberUtil.getUnitOfNumber(Long.parseLong(String.valueOf(e.get("collects")))));
            e.put("likes", NumberUtil.getUnitOfNumber(Long.parseLong(String.valueOf(e.get("likes")))));
        }).collect(Collectors.toList());
    }
}
