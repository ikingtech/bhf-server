package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.SysMenu;
import com.bhf.group.entity.SysRole;
import com.bhf.group.entity.SysRoleMenu;
import com.bhf.group.mapper.SysRoleMapper;
import com.bhf.group.service.SysMenuService;
import com.bhf.group.service.SysRoleMenuService;
import com.bhf.group.service.SysRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Resource
    private SysRoleMenuService sysRoleMenuService;
    @Resource
    private SysMenuService sysMenuService;

    @Override
    public List<SysMenu> getRoleAuthority(String roleId) {
        LambdaQueryWrapper<SysRoleMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysRoleMenu::getRoleId, roleId);
        final List<SysRoleMenu> list = sysRoleMenuService.list(wrapper);
        final List<String> collect = list.stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
        if (collect.size() > 0 && Objects.nonNull(collect)) {
            return sysMenuService.getMenuListByParentId("0", collect);
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> getUserRole(String id) {
        return this.baseMapper.getUserRole(id);
    }
}
