package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.SysStopWord;
import com.bhf.group.mapper.SysStopWordMapper;
import com.bhf.group.service.SysStopWordService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysStopWordServiceImpl extends ServiceImpl<SysStopWordMapper, SysStopWord> implements SysStopWordService {
}
