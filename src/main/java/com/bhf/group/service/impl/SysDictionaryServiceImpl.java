package com.bhf.group.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.SysDictionary;
import com.bhf.group.entity.request.DictionaryRequest;
import com.bhf.group.mapper.SysDictionaryMapper;
import com.bhf.group.service.SysDictionaryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysDictionaryServiceImpl extends ServiceImpl<SysDictionaryMapper, SysDictionary> implements SysDictionaryService {
    @Override
    public Page<SysDictionary> getDictionaryList(DictionaryRequest request) {
        LambdaQueryWrapper<SysDictionary> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysDictionary::getParentId, request.getParentId());
        if (StrUtil.isNotBlank(request.getDescription())) {
            wrapper.like(SysDictionary::getDescription, request.getDescription());
        }
        if (StrUtil.isNotBlank(request.getName())) {
            wrapper.like(SysDictionary::getName, request.getName());
        }
        if (StrUtil.isNotBlank(request.getValue())) {
            wrapper.like(SysDictionary::getValue, request.getValue());
        }
        return this.baseMapper.selectPage(new Page<>(request.getPages(), request.getSize()), wrapper);
    }
}
