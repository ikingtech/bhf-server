package com.bhf.group.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.SysRoleMenu;
import com.bhf.group.mapper.SysRoleMenuMapper;
import com.bhf.group.service.SysRoleMenuService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {
}
