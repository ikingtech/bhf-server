package com.bhf.group.service.impl;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.constants.Constants;
import com.bhf.group.entity.Document;
import com.bhf.group.entity.DocumentCategory;
import com.bhf.group.entity.SysHotWords;
import com.bhf.group.entity.SysNews;
import com.bhf.group.mapper.DocumentCategoryMapper;
import com.bhf.group.service.DocumentCategoryService;
import com.bhf.group.service.DocumentService;
import com.bhf.group.service.SysHotWordsService;
import com.bhf.group.service.SysNewsService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class DocumentCategoryServiceImpl extends ServiceImpl<DocumentCategoryMapper, DocumentCategory> implements DocumentCategoryService {

    @Resource
    private DocumentService documentService;
    @Resource
    private SysNewsService sysNewsService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private SysHotWordsService sysHotWordsService;

    @Override
    public List<DocumentCategory> getDocumentCategoryList(String parentId) {
        final String details = redisTemplate.opsForValue().get(Constants.DATA_CATEGORY_DETAILS);
        if (StrUtil.isNotBlank(details)) {
            return JSON.parseArray(details, DocumentCategory.class);
        } else {
            LambdaQueryWrapper<DocumentCategory> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(DocumentCategory::getParentId, parentId).orderByAsc(DocumentCategory::getSort);
            final List<DocumentCategory> documentCategories = this.baseMapper.selectList(wrapper);
            if (documentCategories.size() > 0 && Objects.nonNull(documentCategories)) {
                for (DocumentCategory documentCategory : documentCategories) {
                    documentCategory.setChildren(getChildrenDocumentCategory(documentCategory.getId()));
                }
            }
            redisTemplate.opsForValue().set(Constants.DATA_CATEGORY_DETAILS, JSON.toJSONString(documentCategories));
            return documentCategories;
        }
    }

    private List<DocumentCategory> getChildrenDocumentCategory(String parentId) {
        List<DocumentCategory> newDocumentCategoryList = new ArrayList<>();
        LambdaQueryWrapper<DocumentCategory> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(DocumentCategory::getParentId, parentId).orderByAsc(DocumentCategory::getSort);
        final List<DocumentCategory> list = this.baseMapper.selectList(wrapper);
        if (list.size() > 0 && Objects.nonNull(list)) {
            for (DocumentCategory category : list) {
                category.setChildren(getChildrenDocumentCategory(category.getId()));
                newDocumentCategoryList.add(category);
            }
        }
        return newDocumentCategoryList;
    }

    @Override
    public List<DocumentCategory> getDocumentCategoryId(String documentCategoryId) {
        final DocumentCategory category = this.baseMapper.selectById(documentCategoryId);
        final List<DocumentCategory> list = getParentDocumentCategory(category.getParentId());
        if (Objects.nonNull(category)) {
            list.add(category);
        }
        return list;
    }

    private List<DocumentCategory> getParentDocumentCategory(String parentId) {
        List<DocumentCategory> list = new ArrayList<>();
        final DocumentCategory category = this.baseMapper.selectById(parentId);
        if (Objects.nonNull(category)) {
            list.add(category);
            getParentDocumentCategory(category.getParentId());
        }
        return list;
    }


    @Override
    public Map<String, Object> getHome() {
        Map<String, Object> result = new HashMap<>(3);
        final String cate = redisTemplate.opsForValue().get(Constants.DATA_CATEGORY);
        if (StrUtil.isNotBlank(cate)) {
            result.put("category", JSON.parse(cate));
        } else {
            LambdaQueryWrapper<DocumentCategory> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(DocumentCategory::getParentId, "0").orderByAsc(DocumentCategory::getSort);
            final List<DocumentCategory> list = this.baseMapper.selectList(wrapper);
            if (Objects.nonNull(list) && list.size() > 0) {
                for (DocumentCategory category : list) {
                    LambdaQueryWrapper<Document> documentLambdaQueryWrapper = new LambdaQueryWrapper<>();
                    documentLambdaQueryWrapper.select(Document::getId, Document::getTitle, Document::getPublishTime).eq(Document::getApprovalStatus, 2)
                            .eq(Document::getDocumentCategoryParentId, category.getId())
                            .eq(Document::getStatus, 0)
                            .orderByDesc(Document::getPublishTime);
                    final List<Map<String, Object>> listMaps = documentService.pageMaps(new Page<>(1, 10), documentLambdaQueryWrapper).getRecords();
                    final List<Map<String, Object>> collect = listMaps.stream().peek(val -> val.put("id", String.valueOf(val.get("id")))).collect(Collectors.toList());
                    category.setDocuments(collect);
                }
            }
            result.put("category", list);
            redisTemplate.opsForValue().set(Constants.DATA_CATEGORY, JSON.toJSONString(list));
        }
        final String news = redisTemplate.opsForValue().get(Constants.NEWS);
        if (StrUtil.isNotBlank(news)) {
            result.put("news", JSON.parse(news));
        } else {
            LambdaQueryWrapper<SysNews> newsLambdaQueryWrapper = new LambdaQueryWrapper<>();
            newsLambdaQueryWrapper.select(SysNews::getId, SysNews::getTitle, SysNews::getPublishTime).eq(SysNews::getStatus, 0).eq(SysNews::getType, 1).orderByDesc(SysNews::getPublishTime);
            final List<Map<String, Object>> records = sysNewsService.pageMaps(new Page<>(1, 10), newsLambdaQueryWrapper).getRecords();
            final List<Map<String, Object>> newsCollect = records.stream().peek(val -> val.put("id", String.valueOf(val.get("id")))).collect(Collectors.toList());
            result.put("news", newsCollect);
            redisTemplate.opsForValue().set(Constants.NEWS, JSON.toJSONString(newsCollect));
        }
        final String s = redisTemplate.opsForValue().get(Constants.HOTS_KEWS);
        if (StrUtil.isNotBlank(s)){
            result.put("hots",JSON.parse(s));
        }else {
            LambdaQueryWrapper<SysHotWords> wrapper = new LambdaQueryWrapper<>();
            wrapper.orderByDesc(SysHotWords::getNums);
            final Page<SysHotWords> page = sysHotWordsService.page(new Page<>(1, 40), wrapper);
            redisTemplate.opsForValue().set(Constants.HOTS_KEWS,JSON.toJSONString(page.getRecords()),Duration.ofMinutes(10));
            result.put("hots",page.getRecords());
        }
        return result;
    }

    @Override
    public List<String> getDocumentCategoryListById(String id) {
        final String s = redisTemplate.opsForValue().get(Constants.DATA_CATEGORY_ID + id);
        if (StrUtil.isNotBlank(s)) {
            return JSON.parseArray(s, String.class);
        }
        List<String> list = new ArrayList<>();
        list.add(id);
        LambdaQueryWrapper<DocumentCategory> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(DocumentCategory::getParentId, id);
        final List<DocumentCategory> categories = this.baseMapper.selectList(wrapper);
        if (categories.size() > 0 && Objects.nonNull(categories)) {
            for (DocumentCategory category : categories) {
                list.add(category.getId());
                list.addAll(getMenuListByChildId(category.getId()));
            }
        }
        redisTemplate.opsForValue().set(Constants.DATA_CATEGORY_ID + id, JSON.toJSONString(list), Duration.ofHours(12));
        return list;
    }

    @Override
    public void subDocumentCategoryNums(List<String> collect) {
        for (String id : collect) {
            this.baseMapper.subDocumentCategoryNums(id);
        }
    }

    public List<String> getMenuListByChildId(String parentId) {
        List<String> strings = new ArrayList<>();
        LambdaQueryWrapper<DocumentCategory> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(DocumentCategory::getParentId, parentId);
        final List<DocumentCategory> list = this.baseMapper.selectList(wrapper);
        if (list.size() > 0 && Objects.nonNull(list)) {
            for (DocumentCategory category : list) {
                strings.add(category.getId());
                getMenuListByChildId(category.getId());
            }
        }
        return strings;
    }
}


