package com.bhf.group.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bhf.group.entity.Document;
import com.bhf.group.entity.request.DocumentApprovalLogRequest;
import com.bhf.group.entity.request.DocumentRequest;
import com.bhf.group.entity.request.IndexFindRequest;
import com.bhf.group.utils.RestResult;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
public interface DocumentService extends IService<Document> {
    Boolean addDocument(Document document);

    Boolean editDocument(Document document);

    Page<Document> getDocumentList(DocumentRequest request, List<Integer> status);

    Boolean documentApproval(String id, String userId);

    Page<Map<String, Object>> getDocumentApprovalLogList(DocumentApprovalLogRequest request);

    Page<Map<String, Object>> findIndexDocumentData(IndexFindRequest request);

    RestResult<Object> getDocumentDetailsById(String id, String userId) throws IOException;

    boolean likeDocument(String id, String userId);

    boolean collectDocument(String id, String userId);
}
