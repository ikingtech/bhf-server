package com.bhf.group.async;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bhf.group.entity.SysHotWords;
import com.bhf.group.entity.UserOperationLog;
import com.bhf.group.service.SysHotWordsService;
import com.bhf.group.service.UserOperationLogService;
import com.bhf.group.utils.TokenizerUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

/**
 * @author pwq
 */
@Component
@Slf4j
public class AsyncTask {

    @Resource
    private UserOperationLogService userOperationLogService;
    @Resource
    private SysHotWordsService sysHotWordsService;
    @Resource
    private TokenizerUtil tokenizerUtil;

    /**
     * 异步插入日志信息
     *
     * @param operationLog
     */
    @Async
    public void insertUserOperationLog(UserOperationLog operationLog) {
        if (StrUtil.isNotBlank(operationLog.getUserId())){
            userOperationLogService.save(operationLog);
        }
        log.info("线程的名字：{}", Thread.currentThread().getName());
    }

    public void insertHotsWords(String title) throws IOException {
        final List<String> list = tokenizerUtil.smartChineseAnalyzer(title);
        if (Objects.nonNull(list) && list.size()>0){
            for (String s : list) {
                LambdaQueryWrapper<SysHotWords> wrapper = new LambdaQueryWrapper<>();
                wrapper.eq(SysHotWords::getWord,s);
                final SysHotWords one = sysHotWordsService.getOne(wrapper);
                if (Objects.nonNull(one)){
                    sysHotWordsService.updateNums(one.getId());
                }else {
                    SysHotWords words = new SysHotWords();
                    words.setWord(s);
                    words.setNums(1);
                    sysHotWordsService.save(words);
                }
            }
        }
    }
}
