package com.bhf.group.aspect;

import com.bhf.group.async.AsyncTask;
import com.bhf.group.constants.Constants;
import com.bhf.group.entity.UserOperationLog;
import com.bhf.group.utils.IpAddressUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

/**
 * @author pwq
 */
@Aspect
@Component
public class OperationLogAspect {

    private static final String START_TIME = "request-start";

    @Resource
    private AsyncTask asyncTask;

    @Pointcut(value = "@annotation(com.bhf.group.aspect.OperationLog)")
    public void operationLogCut() {
    }

    @Before("operationLogCut()")
    public void beforeLog() {
        ServletRequestAttributes startAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest startRequest = Objects.requireNonNull(startAttributes).getRequest();
        startRequest.setAttribute(START_TIME, System.currentTimeMillis());
    }

    /**
     * 记录操作日志
     *
     * @param joinPoint 方法的执行点
     * @param result    方法返回值
     * @throws Throwable
     */
    @AfterReturning(returning = "result", value = "operationLogCut()")
    public void saveOperationLog(JoinPoint joinPoint, Object result) {
        commonOperationLog(joinPoint, "SUCCESS");
    }

    @AfterThrowing(pointcut = "operationLogCut()", throwing = "a")
    public void exceptionLog(JoinPoint point, Throwable a) {
        commonOperationLog(point, "ERROR");
    }

    private void commonOperationLog(JoinPoint point, String msg) {
        ServletRequestAttributes exceptionAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = Objects.requireNonNull(exceptionAttributes).getRequest();
        final UserOperationLog operationLog = UserOperationLog.builder().build();
        operationLog.setMethod(request.getMethod());
        operationLog.setIp(IpAddressUtil.getIpAdrress(request));
        Long start = (Long) request.getAttribute(START_TIME);
        Long end = System.currentTimeMillis();
        operationLog.setTimeConsuming(Math.toIntExact(end - start));
        MethodSignature signature = (MethodSignature) point.getSignature();
        OperationLog annotation = signature.getMethod().getAnnotation(OperationLog.class);
        if (null != annotation) {
            operationLog.setOperation(annotation.value());
        }
        operationLog.setMethodName(signature.getMethod().getName());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            Map principal = (Map) authentication.getPrincipal();
            operationLog.setUserId(String.valueOf(principal.get(Constants.JTI)));
        }
        operationLog.setResultInfo(msg);
        operationLog.setPathUrl(request.getServletPath());
        asyncTask.insertUserOperationLog(operationLog);
    }
}
