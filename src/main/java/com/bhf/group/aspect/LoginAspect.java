package com.bhf.group.aspect;

import cn.hutool.core.bean.BeanUtil;
import com.bhf.group.async.AsyncTask;
import com.bhf.group.constants.Constants;
import com.bhf.group.entity.UserOperationLog;
import com.bhf.group.utils.IpAddressUtil;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Objects;

/**
 * @author pwq
 */
@Component
@Aspect
@Slf4j
public class LoginAspect {

    private static final String START_TIME = "request-start";

    @Resource
    private AsyncTask asyncTask;

    @Pointcut("execution(* org.springframework.security.oauth2.provider.endpoint.TokenEndpoint.postAccessToken(..))")
    public void pointCut() {
    }

    @Before("pointCut()")
    public void beforeLog() {
        ServletRequestAttributes startAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest startRequest = Objects.requireNonNull(startAttributes).getRequest();
        startRequest.setAttribute(START_TIME, System.currentTimeMillis());
    }

    @AfterThrowing(pointcut = "pointCut()", throwing = "a")
    public void exceptionLog(JoinPoint point, Throwable a) {
        ServletRequestAttributes exceptionAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest exceptionRequest = Objects.requireNonNull(exceptionAttributes).getRequest();
        Long start = (Long) exceptionRequest.getAttribute(START_TIME);
        Long end = System.currentTimeMillis();
        final UserOperationLog operation = UserOperationLog.builder()
                .methodName(point.getSignature().getName())
                .ip(IpAddressUtil.getIpAdrress(exceptionRequest))
                .method(exceptionRequest.getMethod())
                .timeConsuming(Math.toIntExact(end - start))
                .operation("登录")
                .resultInfo("ERROR")
                .pathUrl(exceptionRequest.getServletPath())
                .build();
        asyncTask.insertUserOperationLog(operation);
    }

    @AfterReturning(value = "pointCut()", returning = "o")
    public void afterLog(JoinPoint point, Object o) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = Objects.requireNonNull(attributes).getRequest();
        Long start = (Long) request.getAttribute(START_TIME);
        Long end = System.currentTimeMillis();
        final Map<String, Object> objectMap = BeanUtil.beanToMap(o);
        final DefaultOAuth2AccessToken token = (DefaultOAuth2AccessToken) objectMap.get("body");
        final Map<String, Object> additionalInformation = token.getAdditionalInformation();
        final UserOperationLog operation = UserOperationLog.builder()
                .methodName(point.getSignature().getName())
                .ip(IpAddressUtil.getIpAdrress(request))
                .method(request.getMethod())
                .userId(String.valueOf(additionalInformation.get(Constants.JTI)))
                .timeConsuming(Math.toIntExact(end - start))
                .operation("登录")
                .resultInfo("SUCCESS")
                .pathUrl(request.getServletPath())
                .build();
        asyncTask.insertUserOperationLog(operation);
    }
}
