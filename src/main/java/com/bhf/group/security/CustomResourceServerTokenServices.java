package com.bhf.group.security;

import com.bhf.group.constants.Constants;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.InvalidTokenException;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.ClientRegistrationException;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.util.LinkedHashMap;

/**
 * @author pwq
 */
public class CustomResourceServerTokenServices implements ResourceServerTokenServices {


    private final TokenStore tokenStore;
    private final ClientDetailsService clientDetailsService;
    private final RedisTemplate<String, String> redisTemplate;

    public CustomResourceServerTokenServices(TokenStore tokenStore, ClientDetailsService clientDetailsService, RedisTemplate<String, String> redisTemplate) {
        this.tokenStore = tokenStore;
        this.clientDetailsService = clientDetailsService;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public OAuth2Authentication loadAuthentication(String accessTokenValue) throws AuthenticationException, InvalidTokenException {
        OAuth2AccessToken accessToken = tokenStore.readAccessToken(accessTokenValue);
        if (accessToken == null) {
            throw new InvalidTokenException("Invalid access token: " + accessTokenValue);
        } else if (accessToken.isExpired()) {
            tokenStore.removeAccessToken(accessToken);
            throw new InvalidTokenException("Access token expired: " + accessTokenValue);
        }
        OAuth2Authentication result = tokenStore.readAuthentication(accessToken);
        if (result == null) {
            throw new InvalidTokenException("Invalid access token: " + accessTokenValue);
        }
        if (clientDetailsService != null) {
            String clientId = result.getOAuth2Request().getClientId();
            try {
                clientDetailsService.loadClientByClientId(clientId);
            } catch (ClientRegistrationException e) {
                throw new InvalidTokenException("Client not valid: " + clientId, e);
            }
        }
        final LinkedHashMap details = (LinkedHashMap) result.getPrincipal();
        final Object jti = String.valueOf(details.get(Constants.JTI));
        final String identify = String.valueOf(details.get(Constants.IDENTIFY));
        final String identifyValue = redisTemplate.opsForValue().get(Constants.USER_IDENTIFY + jti);
        if (StringUtils.isEmpty(identifyValue)) {
            throw new InvalidTokenException("凭据信息过期,请重新认证");
        }
        if (!identify.equals(identifyValue)) {
            throw new InvalidTokenException("凭据信息错误或异地登录,请重新认证");
        }
        final String freeze = redisTemplate.opsForValue().get(Constants.USER_FREEZE + jti);
        if (Integer.parseInt(freeze) != 0){
            throw new InvalidTokenException("您的账户被冻结,请及时联系管理员");
        }
        redisTemplate.opsForValue().set(Constants.USER_IDENTIFY + jti,identifyValue, Duration.ofHours(6));
        redisTemplate.opsForValue().set(Constants.USER_FREEZE + jti,freeze, Duration.ofHours(6));
        return result;
    }

    @Override
    public OAuth2AccessToken readAccessToken(String accessToken) {
        return tokenStore.readAccessToken(accessToken);
    }
}
