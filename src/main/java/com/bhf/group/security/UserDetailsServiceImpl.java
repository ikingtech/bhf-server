package com.bhf.group.security;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bhf.group.entity.SysUser;
import com.bhf.group.mapper.SysUserMapper;
import com.bhf.group.security.entity.RoleInfo;
import com.bhf.group.security.entity.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class UserDetailsServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) {
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysUser::getUsername, username);
        final SysUser user = this.baseMapper.selectOne(wrapper);
        if (Objects.isNull(user)) {
            throw new InvalidGrantException("用户不存在,请您注册");
        }
        if (user.getIsFreeze()!=0){
            throw new InvalidGrantException("您的账户被冻结,请联系管理员");
        }
        final List<RoleInfo> roleInfos = this.baseMapper.getRoles(user.getId());
        final List<GrantedAuthority> collectName = roleInfos.stream().map(r -> new SimpleGrantedAuthority(r.getName())).collect(Collectors.toList());
        return User.builder().id(user.getId()).username(username).password(user.getPassword()).isFreeze(user.getIsFreeze())
                .authorities(collectName).build();
    }
}
