package com.bhf.group.security.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.common.exceptions.*;
import org.springframework.security.oauth2.provider.error.WebResponseExceptionTranslator;

/**
 * @author pwq
 */
public class CustomWebResponseExceptionTranslator implements WebResponseExceptionTranslator<OAuth2Exception> {
    @Override
    public ResponseEntity<OAuth2Exception> translate(Exception e) {
        if (e instanceof UnsupportedGrantTypeException) {
            return ResponseEntity.status(200).body(new CustomOauthException("错误的授权类型"));
        }
        if (e instanceof InvalidGrantException) {
            return ResponseEntity.status(200).body(new CustomOauthException(e.getMessage()));
        }
        if (e instanceof InvalidScopeException) {
            return ResponseEntity.status(200).body(new CustomOauthException("错误的scope"));
        }
        if (e instanceof InvalidClientException) {
            return ResponseEntity.status(200).body(new CustomOauthException("未经授权的授权类型"));
        }
        if (e instanceof ClassCastException) {
            return ResponseEntity.status(200).body(new CustomOauthException("无法转换的类型"));
        }
        return ResponseEntity.status(200).body(new CustomOauthException(e.getMessage()));
    }
}
