package com.bhf.group.security.exception;

import com.bhf.group.constants.Constants;
import com.bhf.group.constants.ResultCodeEnum;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * @author pwq
 */
public class CustomOauthExceptionSerializer extends StdSerializer<CustomOauthException> {
    private static final long serialVersionUID = -6264481165283222954L;

    protected CustomOauthExceptionSerializer() {
        super(CustomOauthException.class);
    }

    @Override
    public void serialize(CustomOauthException e, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeNumberField(Constants.CODE, ResultCodeEnum.AUTHENTICATION_ERROR.getCode());
        gen.writeStringField(Constants.MSG, e.getMessage());
        gen.writeEndObject();
    }
}
