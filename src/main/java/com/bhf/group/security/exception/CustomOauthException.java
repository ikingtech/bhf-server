package com.bhf.group.security.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;

/**
 * @author pwq
 */
@JsonSerialize(using = CustomOauthExceptionSerializer.class)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class CustomOauthException extends OAuth2Exception {
    public CustomOauthException(String msg) {
        super(msg);
    }
}
