package com.bhf.group.security.point;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.json.JSONUtil;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.utils.RestResultUtil;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author pwq
 */
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    public final String ERROR_INFO = "Cannot convert access token to JSON";
    public final String AUTHENTICATION_ERROR_INFO = "Full authentication is required to access this resource";

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException {
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding(CharsetUtil.UTF_8);
        final PrintWriter writer = response.getWriter();
        if (ERROR_INFO.equals(e.getMessage())) {
            writer.write(JSONUtil.toJsonStr(RestResultUtil.buildResult(ResultCodeEnum.AUTHENTICATION_INFO_ERROR)));
        } else if (AUTHENTICATION_ERROR_INFO.equals(e.getMessage())) {
            writer.write(JSONUtil.toJsonStr(RestResultUtil.buildResult(ResultCodeEnum.AUTHENTICATION_INFO_RESOURCE)));
        } else if (e instanceof InsufficientAuthenticationException) {
            writer.write(JSONUtil.toJsonStr(RestResultUtil.buildResult(ResultCodeEnum.AUTHENTICATION_INFO_EXPIRED)));
        } else {
            writer.write(JSONUtil.toJsonStr(RestResultUtil.failedWithMsg(ResultCodeEnum.AUTHENTICATION_INFO_ERROR.getCode(), e.getMessage())));
        }
        writer.flush();
        writer.close();
        response.flushBuffer();
    }
}
