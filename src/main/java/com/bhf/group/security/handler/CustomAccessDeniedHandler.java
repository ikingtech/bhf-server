package com.bhf.group.security.handler;

import cn.hutool.json.JSONUtil;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.utils.RestResultUtil;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author pwq
 */
public class CustomAccessDeniedHandler implements AccessDeniedHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException {
        response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
        final PrintWriter writer = response.getWriter();
        writer.write(JSONUtil.toJsonStr(RestResultUtil.buildResult(ResultCodeEnum.PERMISSION_DENIED_ERROR)));
        writer.flush();
        writer.close();
        response.flushBuffer();
    }
}
