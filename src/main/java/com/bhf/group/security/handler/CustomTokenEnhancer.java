package com.bhf.group.security.handler;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.IdUtil;
import com.bhf.group.constants.Constants;
import com.bhf.group.security.entity.User;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author pwq
 */
public class CustomTokenEnhancer implements TokenEnhancer {

    public RedisTemplate<String, String> redisTemplate;

    public CustomTokenEnhancer(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        final Map<String, Object> additionalInfo = new HashMap<>(3);
        final User user = (User) authentication.getPrincipal();
        additionalInfo.put(Constants.CODE, 200);
        additionalInfo.put(Constants.JTI, String.valueOf(user.getId()));
        additionalInfo.put(Constants.MSG, "请求成功");
        final String s = IdUtil.objectId();
        additionalInfo.put(Constants.IDENTIFY, s);
        final Date expiration = accessToken.getExpiration();
        long betweenDay = DateUtil.between(expiration, new Date(), DateUnit.SECOND);
        redisTemplate.opsForValue().set(Constants.USER_IDENTIFY + user.getId(), s, Duration.ofSeconds(betweenDay));
        redisTemplate.opsForValue().set(Constants.USER_FREEZE + user.getId(), String.valueOf(user.getIsFreeze()), Duration.ofSeconds(betweenDay));
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);
        return accessToken;
    }
}
