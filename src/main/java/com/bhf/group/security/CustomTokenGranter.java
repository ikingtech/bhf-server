package com.bhf.group.security;

import com.bhf.group.constants.Constants;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.common.exceptions.InvalidGrantException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author pwq
 */
public class CustomTokenGranter extends AbstractTokenGranter {

    private static final String GRANT_TYPE = "captcha_code";

    private final AuthenticationManager authenticationManager;

    private final RedisTemplate<String, String> redisTemplate;

    public CustomTokenGranter(AuthorizationServerTokenServices tokenServices,
                              ClientDetailsService clientDetailsService,
                              OAuth2RequestFactory requestFactory,
                              AuthenticationManager authenticationManager, RedisTemplate<String, String> redisTemplate) {
        super(tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
        this.authenticationManager = authenticationManager;
        this.redisTemplate = redisTemplate;
    }

    @Override
    protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
        Map<String, String> parameters = new LinkedHashMap<>(tokenRequest.getRequestParameters());
        String username = parameters.get("username");
        String password = parameters.get("password");
        String code = parameters.get("code");
        if (StringUtils.isEmpty(code)) {
            throw new InvalidGrantException("验证码不能为空");
        }
        final String s1 = code.toUpperCase().trim();
        final String value = redisTemplate.opsForValue().get(Constants.LOGIN_CAPTCHA + s1);
        if (StringUtils.isEmpty(value)) {
            throw new InvalidGrantException("验证码错误或过期");
        }
        parameters.remove("password");
        Authentication userAuth = new UsernamePasswordAuthenticationToken(username, password);
        ((AbstractAuthenticationToken) userAuth).setDetails(parameters);
        try {
            userAuth = authenticationManager.authenticate(userAuth);
        } catch (AccountStatusException | BadCredentialsException ase) {
            throw new InvalidGrantException("用户名或密码错误");
        }
        if (userAuth == null || !userAuth.isAuthenticated()) {
            throw new InvalidGrantException("Could not authenticate user: " + username);
        }
        OAuth2Request storedOauth2Request = getRequestFactory().createOAuth2Request(client, tokenRequest);
        redisTemplate.delete(Constants.LOGIN_CAPTCHA + s1);
        return new OAuth2Authentication(storedOauth2Request, userAuth);
    }
}
