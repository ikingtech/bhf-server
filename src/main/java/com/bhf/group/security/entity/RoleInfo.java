package com.bhf.group.security.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author pwq
 */
@Data
public class RoleInfo implements Serializable {

    private String name;
    private String id;
}
