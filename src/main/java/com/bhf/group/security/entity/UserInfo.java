package com.bhf.group.security.entity;

import cn.hutool.core.annotation.Alias;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author pwq
 */
@Data
public class UserInfo implements Serializable {
    private static final long serialVersionUID = -5222933404148686719L;
    @Alias("jti")
    private String id;
    @Alias("user_name")
    private String username;
    private ArrayList<String> authorities;
}
