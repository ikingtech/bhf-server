package com.bhf.group.controller;

import cn.hutool.captcha.generator.RandomGenerator;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.constants.Constants;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.core.BaseController;
import com.bhf.group.entity.SysDictionary;
import com.bhf.group.entity.SysMenu;
import com.bhf.group.entity.SysUser;
import com.bhf.group.entity.request.SelfRestPasswordRequest;
import com.bhf.group.entity.request.SysUserRequest;
import com.bhf.group.entity.request.UserLogsRequest;
import com.bhf.group.entity.request.UserRequest;
import com.bhf.group.security.entity.UserInfo;
import com.bhf.group.service.SysDictionaryService;
import com.bhf.group.service.SysRoleService;
import com.bhf.group.service.SysUserService;
import com.bhf.group.service.UserOperationLogService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/user")
@Api(tags = {"用户模块"})
public class SysUserController extends BaseController {

    @Resource
    private UserOperationLogService logService;
    @Resource
    private SysUserService sysUserService;
    @Resource
    private SysRoleService sysRoleService;
    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private SysDictionaryService sysDictionaryService;
    @Resource
    private RedisTemplate<String,String> redisTemplate;

    public static final String[] STRINGS = {"A", "B", "C", "D", "E", "F", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
    public static final String[] STRINGS_LOWER = {"a", "b", "c", "d", "e", "f", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};

    @GetMapping("/info")
    @ApiOperation(value = "用户信息")
    public RestResult<Map<String, Object>> userInfo() {
        final UserInfo user = this.getUser();
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysUser::getId, user.getId()).select(SysUser::getId,SysUser::getUsername,SysUser::getSex,
                SysUser::getSignature,SysUser::getDepartment,SysUser::getCertificate,SysUser::getCertificateType,SysUser::getTelephone,
                SysUser::getAvatar,SysUser::getUnit,SysUser::getIsFreeze,SysUser::getUserLevel,SysUser::getCreateTime);
        final String id = user.getId();
        List<Map<String, Object>> list = sysRoleService.getUserRole(user.getId());
        final SysUser sysUser = sysUserService.getOne(wrapper);
        Map<String, Object> map = BeanUtil.beanToMap(sysUser);
        map.put("role", list);
        return RestResultUtil.success(map);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除用户")
    public RestResult<Boolean> delete(@PathVariable("id") String id){
        redisTemplate.delete(Constants.USER_IDENTIFY+id);
        return RestResultUtil.success(sysUserService.removeById(id));
    }

    @GetMapping("/freeze/{id}")
    @ApiOperation(value = "冻结用户")
    public RestResult<Boolean> freeze(@PathVariable("id") String id){
        final SysUser user = sysUserService.getById(id);
        if (user.getIsFreeze() !=0){
            user.setIsFreeze(0);
            redisTemplate.opsForValue().set(Constants.USER_IDENTIFY+id,"0");
        }else {
            user.setIsFreeze(1);
            redisTemplate.opsForValue().set(Constants.USER_IDENTIFY+id,"1");
        }
        return RestResultUtil.success(sysUserService.updateById(user));
    }

    @GetMapping("/authority")
    @ApiOperation(value = "用户权限")
    public RestResult<List<SysMenu>> userAuthority() {
        final UserInfo user = this.getUser();
        return RestResultUtil.success(sysUserService.getUserAuthority(user.getId()));
    }

    @PostMapping("/logs")
    @ApiOperation(value = "日志查询")
    public RestResult<IPage<Map<String, Object>>> geUserLogs(@RequestBody UserLogsRequest request) {
        return RestResultUtil.success(logService.getUserOperationLogs(request));
    }

    @PostMapping("/list")
    @ApiOperation(value = "用户列表")
    public RestResult<Page<SysUser>> getUserList(@RequestBody UserRequest request) {
        return RestResultUtil.success(sysUserService.getUserList(request));
    }

    @PostMapping("/collects")
    @ApiOperation(value = "用户收藏")
    public RestResult<Page<Map<String, String>>> collects(@RequestBody com.bhf.group.core.Page page) {
        final String id = this.getUser().getId();
        return RestResultUtil.success(sysUserService.getUserCollectsList(id, page));
    }

    @PostMapping("/previews")
    @ApiOperation(value = "用户预览")
    public RestResult<Page<Map<String, String>>> previews(@RequestBody com.bhf.group.core.Page page) {
        final String id = this.getUser().getId();
        return RestResultUtil.success(sysUserService.getUserPreviewsList(id, page));
    }

    @GetMapping("/role/{userId}")
    @ApiOperation(value = "用户角色")
    public RestResult<List<Map<String, Object>>> roleByUserId(@PathVariable("userId") String userId) {
        return RestResultUtil.success(sysRoleService.getUserRole(userId));
    }

    @GetMapping("/level/{userId}/{level}")
    @ApiOperation(value = "修改用户级别")
    public RestResult<Boolean> levelByUserId(@PathVariable("userId") String userId, @PathVariable("level") String level) {
        LambdaQueryWrapper<SysDictionary> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysDictionary::getValue, level);
        final int count = sysDictionaryService.count(wrapper);
        if (count > 0) {
            SysUser user = new SysUser();
            user.setUserLevel(level);
            user.setId(userId);
            return RestResultUtil.success(sysUserService.updateById(user));
        }
        return RestResultUtil.failed();
    }

    @GetMapping("/reset/password/{userId}")
    @ApiOperation(value = "重置密码")
    public RestResult<String> restPassword(@PathVariable("userId") String userId) {
        String s = RandomUtil.randomInt(1000, 10000) + CollUtil.join(RandomUtil.randomEleSet(CollUtil.newArrayList(STRINGS), 3)
                , "") + CollUtil.join(RandomUtil.randomEleSet(CollUtil.newArrayList(STRINGS_LOWER), 3), "");
        System.out.println(s);
        RandomGenerator randomGenerator = new RandomGenerator(s, 10);
        final String generate = randomGenerator.generate();
        SysUser user = new SysUser();
        user.setId(userId);
        user.setPassword(passwordEncoder.encode(generate));
        if (sysUserService.updateById(user)) {
            return RestResultUtil.success(generate);
        }
        return RestResultUtil.failed();
    }

    @PostMapping("/reset/password")
    @ApiOperation(value = "个人中心修改密码")
    public RestResult<Object> selfRestPassword(@Validated @RequestBody SelfRestPasswordRequest request) {
        final SysUser user = sysUserService.getById(this.getUser().getId());
        if (!passwordEncoder.matches(request.getOldPassword(), user.getPassword())) {
            return RestResultUtil.buildResult(ResultCodeEnum.PASSWORD_DIFFERENT_ERROR);
        }
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        return RestResultUtil.success(sysUserService.updateById(user));
    }

    @PostMapping("/edit/self/info")
    @ApiOperation(value = "修改个人信息")
    public RestResult<Object> editEelfInfo(@Validated @RequestBody SysUserRequest request) {
        SysUser user = new SysUser();
        user.setId(this.getUser().getId());
        user.setUsername(request.getUsername());
        user.setDepartment(request.getDepartment());
        user.setSignature(request.getSignature());
        user.setSex(request.getSex());
        user.setUnit(request.getUnit());
        user.setAvatar(request.getAvatar());
        user.setCertificate(request.getCertificate());
        user.setCertificateType(request.getCertificateType());
        user.setTelephone(request.getTelephone());
        return RestResultUtil.success(sysUserService.updateById(user));
    }
}
