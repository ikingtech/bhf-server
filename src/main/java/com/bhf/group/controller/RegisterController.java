package com.bhf.group.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.CircleCaptcha;
import cn.hutool.captcha.generator.RandomGenerator;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NumberUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bhf.group.constants.Constants;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.entity.SysDictionary;
import com.bhf.group.entity.SysRole;
import com.bhf.group.entity.SysUser;
import com.bhf.group.entity.SysUserRole;
import com.bhf.group.service.SysDictionaryService;
import com.bhf.group.service.SysRoleService;
import com.bhf.group.service.SysUserRoleService;
import com.bhf.group.service.SysUserService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/register")
@Api(tags = {"注册信息"})
public class RegisterController {

    public static final String BASE_STR = "ABCDEFGHJKNPRSTUXYZ0123456789";

    @Resource
    private RedisTemplate<String, String> redisTemplate;
    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private SysUserService sysUserService;
    @Resource
    private SysRoleService sysRoleService;
    @Resource
    private SysUserRoleService sysUserRoleService;
    @Resource
    private SysDictionaryService sysDictionaryService;

    @PostMapping("/user/info")
    @ApiOperation(value = "注册用户")
    public RestResult<Boolean> registerUserInfo(@Validated(value = {SysUser.SysUserAdd.class}) @RequestBody SysUser user) {
        final String department = user.getDepartment();
        if (NumberUtil.isNumber(department)) {
            return RestResultUtil.buildResult(ResultCodeEnum.REGISTER_USER_INFO_ERROR);
        }
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysUser::getUsername, user.getUsername());
        if (sysUserService.count(wrapper) > 0) {
            return RestResultUtil.buildResult(ResultCodeEnum.USERNAME_EXIST_ERROR, false);
        }
        user.setUserLevel("inside");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        final boolean save = sysUserService.save(user);
        if (save) {
            LambdaQueryWrapper<SysRole> roleWrapper = new LambdaQueryWrapper<>();
            roleWrapper.eq(SysRole::getEnglishName, "user");
            final SysRole sysRole = sysRoleService.getOne(roleWrapper);
            SysUserRole role = new SysUserRole();
            role.setUserId(user.getId());
            role.setRoleId(sysRole.getId());
            return RestResultUtil.success(sysUserRoleService.save(role));
        }
        return RestResultUtil.buildResult(ResultCodeEnum.REGISTER_USER_ERROR);
    }

    @GetMapping("/captcha")
    @ApiOperation(value = "验证码")
    public void getCaptcha(HttpServletResponse response) throws IOException {
        RandomGenerator randomGenerator = new RandomGenerator(BASE_STR, 4);
        CircleCaptcha captcha = CaptchaUtil.createCircleCaptcha(200, 100, 4, 16);
        captcha.setGenerator(randomGenerator);
        redisTemplate.opsForValue().set(Constants.LOGIN_CAPTCHA + captcha.getCode(), IdUtil.objectId(), Duration.ofSeconds(120));
        final ServletOutputStream outputStream = response.getOutputStream();
        captcha.write(outputStream);
        outputStream.flush();
        outputStream.close();
    }

    @GetMapping("/level/{value}")
    @ApiOperation(value = "获取字典数据")
    public RestResult<List<SysDictionary>> getUserLevel(@PathVariable("value") String value) {
        LambdaQueryWrapper<SysDictionary> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysDictionary::getValue, value);
        wrapper.eq(SysDictionary::getParentId, "0");
        final SysDictionary one = sysDictionaryService.getOne(wrapper);
        LambdaQueryWrapper<SysDictionary> sysDictionaryLambdaQueryWrapper = new LambdaQueryWrapper<>();
        sysDictionaryLambdaQueryWrapper.eq(SysDictionary::getParentId, one.getId()).orderByAsc(SysDictionary::getGrade);
        return RestResultUtil.success(sysDictionaryService.list(sysDictionaryLambdaQueryWrapper));
    }
}
