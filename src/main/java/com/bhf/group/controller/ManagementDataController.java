package com.bhf.group.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.core.BaseController;
import com.bhf.group.entity.Document;
import com.bhf.group.entity.DocumentApprovalLog;
import com.bhf.group.entity.request.DocumentRequest;
import com.bhf.group.entity.request.ManagementDataRequest;
import com.bhf.group.service.DocumentApprovalLogService;
import com.bhf.group.service.DocumentService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/management/data")
@Api(tags = {"数据审核管理"})
public class ManagementDataController extends BaseController {

    @Resource
    private DocumentService documentService;
    @Resource
    private DocumentApprovalLogService documentApprovalLogService;

    @PostMapping("/list")
    @ApiOperation(value = "查询数据")
    public RestResult<Page<Document>> list(@RequestBody DocumentRequest request) {
        if (Objects.nonNull(request.getApprovalStatus())) {
            return RestResultUtil.success(documentService.getDocumentList(request, Collections.singletonList(request.getApprovalStatus())));
        } else {
            return RestResultUtil.success(documentService.getDocumentList(request, Arrays.asList(1, 2, 3)));
        }
    }

    @GetMapping("/freeze/{id}")
    @ApiOperation(value = "冻结数据")
    public RestResult<Boolean> freeze(@PathVariable("id") String id){
        final Document document = documentService.getById(id);
        if (document.getIsFreeze()!=0){
            document.setIsFreeze(0);
        }else {
            document.setIsFreeze(1);
        }
        return RestResultUtil.success(documentService.updateById(document));
    }

    @PostMapping("/approval")
    @ApiOperation(value = "管理员审核数据")
    public RestResult<Object> approval(@Validated @RequestBody ManagementDataRequest request) {
        final DocumentApprovalLog log = new DocumentApprovalLog();
        request.setUserId(this.getUser().getId());
        return RestResultUtil.success(documentApprovalLogService.saveAndDocumentApprovalStatus(request));
    }
}
