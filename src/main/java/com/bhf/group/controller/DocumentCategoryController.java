package com.bhf.group.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bhf.group.constants.Constants;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.entity.DocumentCategory;
import com.bhf.group.service.DocumentCategoryService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/data/category")
@Api(tags = {"数据分类"})
public class DocumentCategoryController {

    @Resource
    private DocumentCategoryService documentCategoryService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @PostMapping("/add")
    @ApiOperation(value = "添加分类")
    public RestResult<Boolean> add(@Validated(value = {DocumentCategory.DocumentCategoryAdd.class}) @RequestBody DocumentCategory category) {
        redisTemplate.delete(Constants.DATA_CATEGORY_DETAILS);
        redisTemplate.delete(Constants.DATA_CATEGORY);
        return RestResultUtil.success(documentCategoryService.save(category));
    }

    @PostMapping("/edit")
    @ApiOperation(value = "编辑分类")
    public RestResult<Boolean> edit(@Validated(value = {DocumentCategory.DocumentCategoryEdit.class}) @RequestBody DocumentCategory category) {
        redisTemplate.delete(Constants.DATA_CATEGORY_DETAILS);
        redisTemplate.delete(Constants.DATA_CATEGORY);
        return RestResultUtil.success(documentCategoryService.updateById(category));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除分类")
    public RestResult<Boolean> delete(@PathVariable("id") String id) {
        LambdaQueryWrapper<DocumentCategory> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(DocumentCategory::getParentId, id);
        final int count = documentCategoryService.count(wrapper);
        if (count > 0) {
            return RestResultUtil.buildResult(ResultCodeEnum.DELETE_CHILDREN_ERROR);
        }
        redisTemplate.delete(Constants.DATA_CATEGORY_DETAILS);
        redisTemplate.delete(Constants.DATA_CATEGORY);
        return RestResultUtil.success(documentCategoryService.removeById(id));
    }

    @GetMapping("/list")
    @ApiOperation(value = "查询分类")
    public RestResult<List<DocumentCategory>> list(@RequestParam(value = "parentId", required = false, defaultValue = "0") String parentId) {
        return RestResultUtil.success(documentCategoryService.getDocumentCategoryList(parentId));
    }

    @GetMapping("/list/{id}")
    @ApiOperation(value = "查询分类1111")
    public RestResult<List<String>> list11(@PathVariable("id") String id) {
        return RestResultUtil.success(documentCategoryService.getDocumentCategoryListById(id));
    }
}
