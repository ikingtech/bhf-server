package com.bhf.group.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.constants.Constants;
import com.bhf.group.core.BaseController;
import com.bhf.group.entity.Document;
import com.bhf.group.entity.DocumentCategory;
import com.bhf.group.entity.request.DocumentApprovalLogRequest;
import com.bhf.group.entity.request.DocumentRequest;
import com.bhf.group.service.DocumentApprovalLogService;
import com.bhf.group.service.DocumentCategoryService;
import com.bhf.group.service.DocumentService;
import com.bhf.group.utils.MinioUtil;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/document")
@Api(tags = {"数据管理"})
public class DocumentController extends BaseController {

    @Resource
    private DocumentService documentService;
    @Resource
    private DocumentApprovalLogService documentApprovalLogService;
    @Resource
    private MinioUtil minioUtil;
    @Resource
    private DocumentCategoryService documentCategoryService;
    @Resource
    private RedisTemplate<String,String> redisTemplate;

    @PostMapping("/add")
    @ApiOperation(value = "添加数据")
    public RestResult<Boolean> add(@Validated(value = {Document.DocumentAdd.class}) @RequestBody Document document) {
        document.setUserId(this.getUser().getId());
        return RestResultUtil.success(documentService.addDocument(document));
    }

    @PostMapping("/edit")
    @ApiOperation(value = "编辑数据")
    public RestResult<Boolean> edit(@Validated(value = {Document.DocumentEdit.class}) @RequestBody Document document) {
        return RestResultUtil.success(documentService.editDocument(document));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除数据")
    public RestResult<Boolean> delete(@PathVariable("id") String id) {
        final Document byId = documentService.getById(id);
        if (byId.getApprovalStatus()==2){
            final List<DocumentCategory> documentCategoryId = documentCategoryService.getDocumentCategoryId(byId.getDocumentCategoryId());
            final List<String> collect = documentCategoryId.stream().map(DocumentCategory::getId).collect(Collectors.toList());
            documentCategoryService.subDocumentCategoryNums(collect);
            redisTemplate.delete(Constants.DATA_CATEGORY_DETAILS);
            redisTemplate.delete(Constants.DATA_CATEGORY);
        }
        return RestResultUtil.success(documentService.removeById(id));
    }

    @PostMapping("/list")
    @ApiOperation(value = "查询数据")
    public RestResult<Page<Document>> list(@RequestBody DocumentRequest request) {
        request.setUserId(this.getUser().getId());
        if (Objects.nonNull(request.getApprovalStatus())){
            return RestResultUtil.success(documentService.getDocumentList(request, Collections.singletonList(request.getApprovalStatus())));
        }
        return RestResultUtil.success(documentService.getDocumentList(request, Arrays.asList(0, 1, 2, 3)));
    }

    @GetMapping("/approval/{id}")
    @ApiOperation(value = "提交审核数据")
    public RestResult<Boolean> approval(@PathVariable("id") String id) {
        final String userId = this.getUser().getId();
        return RestResultUtil.success(documentService.documentApproval(id, userId));
    }

    @PostMapping("/approval/log")
    @ApiOperation(value = "数据审核日志")
    public RestResult<Page<Map<String, Object>>> approvalLog(@Validated @RequestBody DocumentApprovalLogRequest request) {
        return RestResultUtil.success(documentService.getDocumentApprovalLogList(request));
    }

    @PostMapping("/file")
    @ApiOperation(value = "上传文件")
    public RestResult<Object> getFile(MultipartFile file) throws Exception {
        final String test = minioUtil.putObject(Constants.BUCKETS_NAME, file);
        return RestResultUtil.success(test);
    }

    @PostMapping("/files")
    @ApiOperation(value = "批量上传文件")
    public RestResult<Object> uploads(MultipartFile[] file) throws Exception {
        final Map<String, Object> test = minioUtil.putObjects(Constants.BUCKETS_NAME, file);
        return RestResultUtil.success(test);
    }

    @GetMapping("/like/{id}")
    @ApiOperation(value = "用户点赞")
    public RestResult<Boolean> like(@PathVariable("id") String id) {
        final String userId = this.getUser().getId();
        return RestResultUtil.success(documentService.likeDocument(id, userId));
    }

    @GetMapping("/collect/{id}")
    @ApiOperation(value = "用户收藏")
    public RestResult<Boolean> collect(@PathVariable("id") String id) {
        final String userId = this.getUser().getId();
        return RestResultUtil.success(documentService.collectDocument(id, userId));
    }
}
