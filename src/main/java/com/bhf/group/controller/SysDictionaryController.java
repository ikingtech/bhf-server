package com.bhf.group.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.entity.SysDictionary;
import com.bhf.group.entity.request.DictionaryRequest;
import com.bhf.group.service.SysDictionaryService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/dic")
@Api(tags = {"数据字典"})
public class SysDictionaryController {

    @Resource
    private SysDictionaryService dictionaryService;


    @PostMapping("/add")
    @ApiOperation(value = "添加数据")
    public RestResult<Boolean> add(@Validated(value = {SysDictionary.SysDictionaryAdd.class}) @RequestBody SysDictionary dictionary) {
        LambdaQueryWrapper<SysDictionary> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysDictionary::getValue, dictionary.getValue());
        final List<SysDictionary> list = dictionaryService.list(wrapper);
        if (list.size() > 0) {
            return RestResultUtil.buildResult(ResultCodeEnum.DICTIONARY_REPEAT_ERROR);
        }
        return RestResultUtil.success(dictionaryService.save(dictionary));
    }

    @PostMapping("/edit")
    @ApiOperation(value = "编辑数据")
    public RestResult<Boolean> edit(@Validated(value = {SysDictionary.SysDictionaryEdit.class}) @RequestBody SysDictionary dictionary) {
        LambdaQueryWrapper<SysDictionary> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysDictionary::getValue, dictionary.getValue());
        wrapper.and(e -> e.ne(SysDictionary::getId, dictionary.getId()));
        final List<SysDictionary> list = dictionaryService.list(wrapper);
        if (list.size() > 0) {
            return RestResultUtil.buildResult(ResultCodeEnum.DICTIONARY_REPEAT_ERROR);
        }
        return RestResultUtil.success(dictionaryService.updateById(dictionary));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除数据")
    public RestResult<Boolean> delete(@PathVariable("id") Long id) {
        return RestResultUtil.success(dictionaryService.removeById(id));
    }


    @PostMapping("/list")
    @ApiOperation(value = "查询数据")
    public RestResult<Page<SysDictionary>> list(@RequestBody DictionaryRequest request) {
        return RestResultUtil.success(dictionaryService.getDictionaryList(request));
    }

}
