package com.bhf.group.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.entity.DocumentTemplateCategory;
import com.bhf.group.entity.request.DocumentTemplateCategoryRequest;
import com.bhf.group.service.DocumentTemplateCategoryService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/doc/template/category")
@Api(tags = {"模板分类"})
public class DocumentTemplateCategoryController {

    @Resource
    private DocumentTemplateCategoryService documentTemplateCategoryService;

    @PostMapping("/add")
    @ApiOperation(value = "添加分类")
    public RestResult<Boolean> add(@Validated(value = {DocumentTemplateCategory.DocumentTemplateCategoryAdd.class}) @RequestBody DocumentTemplateCategory category) {
        return RestResultUtil.success(documentTemplateCategoryService.save(category));
    }

    @PostMapping("/edit")
    @ApiOperation(value = "编辑分类")
    public RestResult<Boolean> edit(@Validated(value = {DocumentTemplateCategory.DocumentTemplateCategoryEdit.class}) @RequestBody DocumentTemplateCategory category) {
        return RestResultUtil.success(documentTemplateCategoryService.updateById(category));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "编辑分类")
    public RestResult<Boolean> delete(@PathVariable("id") String id) {
        return RestResultUtil.success(documentTemplateCategoryService.removeById(id));
    }

    @PostMapping("/list")
    @ApiOperation(value = "查询分类")
    public RestResult<Page<DocumentTemplateCategory>> list(@RequestBody DocumentTemplateCategoryRequest request) {
        LambdaQueryWrapper<DocumentTemplateCategory> wrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(request.getDescription())) {
            wrapper.like(DocumentTemplateCategory::getDescription, request.getDescription());
        }
        if (StrUtil.isNotBlank(request.getName())) {
            wrapper.like(DocumentTemplateCategory::getName, request.getName());
        }
        if (StrUtil.isNotBlank(request.getDocumentCategoryId())) {
            wrapper.eq(DocumentTemplateCategory::getDocumentCategoryId, request.getDocumentCategoryId());
        }
        wrapper.orderByDesc(DocumentTemplateCategory::getCreateTime);
        final Page<DocumentTemplateCategory> page = documentTemplateCategoryService.page(new Page<>(request.getPages(), request.getSize()), wrapper);
        return RestResultUtil.success(page);
    }
}
