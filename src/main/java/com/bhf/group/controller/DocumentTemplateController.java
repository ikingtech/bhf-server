package com.bhf.group.controller;

import com.bhf.group.entity.DocumentTemplate;
import com.bhf.group.service.DocumentTemplateService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import com.bhf.group.utils.Snowflake;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/doc/template")
@Api(tags = {"模板管理"})
public class DocumentTemplateController {

    @Resource
    private DocumentTemplateService documentTemplateService;
    @Resource
    private Snowflake snowflake;


    @PostMapping("/addOrEdit/{categoryId}")
    @ApiOperation(value = "添加或编辑模板")
    public RestResult<Boolean> add(@Validated(value = {DocumentTemplate.DocumentTemplateAdd.class}) @RequestBody List<DocumentTemplate> template,
                                   @PathVariable("categoryId") String categoryId) {
        return RestResultUtil.success(documentTemplateService.addDocumentTemplate(template, categoryId));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除模板")
    public RestResult<Boolean> delete(@PathVariable("id") String id) {
        return RestResultUtil.success(documentTemplateService.removeById(id));
    }

    @GetMapping("/get/{categoryId}")
    @ApiOperation(value = "查询模板")
    public RestResult<List<DocumentTemplate>> get(@PathVariable("categoryId") String categoryId) {
        return RestResultUtil.success(documentTemplateService.getDocumentTemplate(categoryId));
    }
}
