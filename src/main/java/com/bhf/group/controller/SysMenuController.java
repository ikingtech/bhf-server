package com.bhf.group.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.entity.SysMenu;
import com.bhf.group.entity.SysRoleMenu;
import com.bhf.group.entity.request.GrantRoleMenuRequest;
import com.bhf.group.service.SysMenuService;
import com.bhf.group.service.SysRoleMenuService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/menu")
@Api(tags = {"权限管理"})
public class SysMenuController {

    @Resource
    private SysMenuService sysMenuService;
    @Resource
    private SysRoleMenuService sysRoleMenuService;

    @PostMapping("/add")
    @ApiOperation(value = "添加权限")
    public RestResult<Boolean> add(@Validated(value = {SysMenu.MenuAdd.class}) @RequestBody SysMenu menu) {
        LambdaQueryWrapper<SysMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.or().eq(SysMenu::getName, menu.getName()).or().eq(SysMenu::getEnglishName, menu.getEnglishName());
        final long count = sysMenuService.list(wrapper).size();
        if (count > 0) {
            return RestResultUtil.buildResult(ResultCodeEnum.ROLE_REPEAT_ERROR);
        }
        return RestResultUtil.success(sysMenuService.save(menu));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除权限")
    public RestResult<Boolean> delete(@PathVariable("id") String id) {
        return RestResultUtil.success(sysMenuService.removeById(id));
    }

    @PostMapping("/edit")
    @ApiOperation(value = "编辑权限")
    public RestResult<Boolean> edit(@Validated(value = {SysMenu.MenuEdit.class}) @RequestBody SysMenu menu) {
        LambdaQueryWrapper<SysMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(SysMenu::getId, menu.getId()).and(w -> w.or().eq(SysMenu::getName, menu.getName()).or().eq(SysMenu::getEnglishName, menu.getEnglishName()));
        final long count = sysMenuService.list(wrapper).size();
        if (count > 0) {
            return RestResultUtil.buildResult(ResultCodeEnum.ROLE_REPEAT_ERROR);
        }
        return RestResultUtil.success(sysMenuService.updateById(menu));
    }

    @GetMapping("/tree")
    @ApiOperation(value = "查询权限")
    public RestResult<List<SysMenu>> menuTree(@RequestParam(name = "parentId", defaultValue = "0", required = false) String parentId) {
        return RestResultUtil.success(sysMenuService.getMenuListByParentId(parentId));
    }

    @PostMapping("/grant/role")
    @ApiOperation(value = "授予角色权限")
    public RestResult<Boolean> grantMenuRole(@Validated @RequestBody GrantRoleMenuRequest request) {
        LambdaQueryWrapper<SysRoleMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysRoleMenu::getRoleId, request.getRoleId());
        sysRoleMenuService.remove(wrapper);
        if (request.getMenuIds().length <= 0) {
            return RestResultUtil.success(true);
        }
        return RestResultUtil.success(sysRoleMenuService.saveBatch(Arrays.stream(request.getMenuIds()).map(u -> {
            SysRoleMenu menu = new SysRoleMenu();
            menu.setMenuId(u);
            menu.setRoleId(request.getRoleId());
            return menu;
        }).collect(Collectors.toList())));
    }
}
