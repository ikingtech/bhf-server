package com.bhf.group.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.constants.Constants;
import com.bhf.group.core.BaseController;
import com.bhf.group.entity.SysNews;
import com.bhf.group.entity.request.NewsRequest;
import com.bhf.group.service.SysNewsService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/news")
@Api(tags = {"资讯管理"})
public class SysNewsController extends BaseController {

    @Resource
    private SysNewsService sysNewsService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @PostMapping("/add")
    @ApiOperation(value = "添加资讯")
    public RestResult<Boolean> add(@Validated(value = {SysNews.SysNewsAdd.class}) @RequestBody SysNews news) {
        news.setCreateUser(this.getUser().getId());
        if (news.getType() == 1) {
            news.setPublishTime(LocalDateTime.now());
            redisTemplate.delete(Constants.NEWS);
        }
        return RestResultUtil.success(sysNewsService.save(news));
    }

    @PostMapping("/edit")
    @ApiOperation(value = "编辑资讯")
    public RestResult<Boolean> edit(@Validated(value = {SysNews.SysNewsEdit.class}) @RequestBody SysNews news) {
        news.setCreateUser(this.getUser().getId());
        if (news.getType() == 1) {
            news.setPublishTime(LocalDateTime.now());
            redisTemplate.delete(Constants.NEWS);
        }
        return RestResultUtil.success(sysNewsService.updateById(news));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除资讯")
    public RestResult<Boolean> delete(@PathVariable("id") Long id) {
        redisTemplate.delete(Constants.NEWS);
        return RestResultUtil.success(sysNewsService.removeById(id));
    }

    @DeleteMapping("/publish/{id}")
    @ApiOperation(value = "发布资讯")
    public RestResult<Boolean> publish(@PathVariable("id") String id) {
        SysNews news = new SysNews();
        news.setId(id);
        news.setType(1);
        news.setPublishTime(LocalDateTime.now());
        redisTemplate.delete(Constants.NEWS);
        return RestResultUtil.success(sysNewsService.updateById(news));
    }

    @PostMapping("/list")
    @ApiOperation(value = "查询资讯")
    public RestResult<Page<SysNews>> list(@RequestBody NewsRequest request) {
        LambdaQueryWrapper<SysNews> wrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(request.getTitle())) {
            wrapper.like(SysNews::getTitle, request.getTitle());
        }
        wrapper.orderByDesc(SysNews::getCreateTime);
        final Page<SysNews> page = sysNewsService.page(new Page<>(request.getPages(), request.getSize()), wrapper);
        return RestResultUtil.success(page);
    }

}
