package com.bhf.group.controller;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.constants.Constants;
import com.bhf.group.constants.ResultCodeEnum;
import com.bhf.group.entity.SysMenu;
import com.bhf.group.entity.SysRole;
import com.bhf.group.entity.SysRoleMenu;
import com.bhf.group.entity.SysUserRole;
import com.bhf.group.entity.request.GrantUserRoleRequest;
import com.bhf.group.entity.request.RoleRequest;
import com.bhf.group.service.SysRoleMenuService;
import com.bhf.group.service.SysRoleService;
import com.bhf.group.service.SysUserRoleService;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/role")
@Api(tags = {"角色管理"})
public class SysRoleController {

    @Resource
    private SysRoleService roleService;
    @Resource
    private SysUserRoleService sysUserRoleService;
    @Resource
    private SysRoleMenuService sysRoleMenuService;

    @PostMapping("/add")
    @ApiOperation(value = "添加角色")
    public RestResult<Boolean> add(@Validated(value = {SysRole.RoleAdd.class}) @RequestBody SysRole role) {
        LambdaQueryWrapper<SysRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.or().eq(SysRole::getName, role.getName()).or().eq(SysRole::getEnglishName, role.getEnglishName());
        final int count = roleService.count(wrapper);
        if (count > 0) {
            return RestResultUtil.buildResult(ResultCodeEnum.ROLE_REPEAT_ERROR);
        }
        return RestResultUtil.success(roleService.save(role));
    }

    @PostMapping("/edit")
    @ApiOperation(value = "编辑角色")
    public RestResult<Boolean> edit(@Validated(value = {SysRole.RoleEdit.class}) @RequestBody SysRole role) {
        LambdaQueryWrapper<SysRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(SysRole::getId, role.getId());
        wrapper.and(w -> w.or().eq(SysRole::getName, role.getName()).or().eq(SysRole::getEnglishName, role.getEnglishName()));
        final long count = roleService.list(wrapper).size();
        if (count > 0) {
            return RestResultUtil.buildResult(ResultCodeEnum.ROLE_REPEAT_ERROR);
        }
        return RestResultUtil.success(roleService.updateById(role));
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation(value = "删除角色")
    public RestResult<Boolean> delete(@PathVariable(value = "id") Long id) {
        final SysRole role = roleService.getById(id);
        if (ArrayUtil.contains(Constants.ROLES, role.getName())) {
            return RestResultUtil.buildResult(ResultCodeEnum.ROLE_NOT_DELETE_ERROR);
        }
        LambdaQueryWrapper<SysUserRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysUserRole::getRoleId, id);
        sysUserRoleService.remove(wrapper);
        LambdaQueryWrapper<SysRoleMenu> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SysRoleMenu::getRoleId, id);
        sysRoleMenuService.remove(queryWrapper);
        return RestResultUtil.success(roleService.removeById(id));
    }

    @PostMapping("/list")
    @ApiOperation(value = "查询角色")
    public RestResult<Page<SysRole>> list(@RequestBody RoleRequest request) {
        LambdaQueryWrapper<SysRole> wrapper = new LambdaQueryWrapper<>();
        if (StrUtil.isNotBlank(request.getName())) {
            wrapper.eq(SysRole::getName, request.getName());
        }
        if (StrUtil.isNotBlank(request.getEnglishName())) {
            wrapper.eq(SysRole::getEnglishName, request.getEnglishName());
        }
        wrapper.orderByDesc(SysRole::getCreateTime);
        final Page<SysRole> page = roleService.page(new Page<>(request.getPages(), request.getSize()), wrapper);
        return RestResultUtil.success(page);
    }

    @PostMapping("/grant/user")
    @ApiOperation(value = "授予用户角色")
    public RestResult<Boolean> userGrantRole(@Validated @RequestBody GrantUserRoleRequest request) {
        List<SysUserRole> list = new ArrayList<>();
        for (String roleId : request.getRoleIds()) {
            SysUserRole userRole = new SysUserRole();
            userRole.setRoleId(roleId);
            userRole.setUserId(request.getUserId());
            list.add(userRole);
        }
        LambdaQueryWrapper<SysUserRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysUserRole::getUserId, request.getUserId());
        sysUserRoleService.remove(wrapper);
        return RestResultUtil.success(sysUserRoleService.saveBatch(list));
    }

    @GetMapping("/authority/{roleId}")
    @ApiOperation(value = "角色权限")
    public RestResult<List<SysMenu>> authority(@PathVariable("roleId") String roleId) {
        return RestResultUtil.success(roleService.getRoleAuthority(roleId));
    }
}
