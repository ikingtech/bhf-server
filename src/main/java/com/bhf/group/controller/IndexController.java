package com.bhf.group.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.async.AsyncTask;
import com.bhf.group.core.BaseController;
import com.bhf.group.entity.DocumentCategory;
import com.bhf.group.entity.request.IndexFindRequest;
import com.bhf.group.service.*;
import com.bhf.group.utils.RestResult;
import com.bhf.group.utils.RestResultUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
@RestController
@RequestMapping("/index")
@Api(tags = {"首页"})
public class IndexController extends BaseController {

    @Resource
    private DocumentCategoryService documentCategoryService;
    @Resource
    private DocumentService documentService;
    @Resource
    private SysStopWordService stopWordService;
    @Resource
    private SysNewsService sysNewsService;
    @Resource
    private AsyncTask asyncTask;
    @Resource
    private DocumentAnnexService documentAnnexService;

    @GetMapping("/home")
    @ApiOperation(value = "首页数据")
    public RestResult<Map<String, Object>> home() {
        return RestResultUtil.success(documentCategoryService.getHome());
    }


    @GetMapping("/data/category")
    @ApiOperation(value = "首页数据分类")
    public RestResult<List<DocumentCategory>> getDataCategory(@RequestParam(value = "parentId", required = false, defaultValue = "0") String parentId) {
        return RestResultUtil.success(documentCategoryService.getDocumentCategoryList(parentId));
    }

    @PostMapping("/find/data")
    @ApiOperation(value = "查询首页数据")
    public RestResult<Page<Map<String, Object>>> getDataCategory(@Validated @RequestBody IndexFindRequest request) throws IOException {
        if (StrUtil.isNotBlank(request.getTitle().getTitle())){
            asyncTask.insertHotsWords(request.getTitle().getTitle());
        }
        return RestResultUtil.success(documentService.findIndexDocumentData(request));
    }

    @GetMapping("/data/details/{id}")
    @ApiOperation(value = "查看数据详情")
    public RestResult<Object> details(@PathVariable("id") String id) throws IOException {
        final String userId = this.getUser().getId();
        return documentService.getDocumentDetailsById(id, userId);
    }

    @GetMapping("/news/{id}")
    @ApiOperation(value = "资讯详情")
    public RestResult<Map<String, Object>> news(@PathVariable("id") String id) {
        return RestResultUtil.success(sysNewsService.getNewsDetails(id));
    }

    @PostMapping("/news/list")
    @ApiOperation(value = "资讯列表")
    public RestResult<Object> newsList(@RequestBody com.bhf.group.core.Page page) {
        Page<Map<String, Object>> news = sysNewsService.getNewsList(page);
        return RestResultUtil.success(news);
    }

    @GetMapping("/download/{id}")
    @ApiOperation(value = "下载附件文档")
    public RestResult<Boolean> download(@PathVariable("id") String id) {
        return RestResultUtil.success(documentAnnexService.download(id));
    }

}
