package com.bhf.group.constants;

import lombok.Getter;

/**
 * @author pwq
 */

@Getter
public enum ResultCodeEnum {

    /**
     * Common code.
     **/
    SUCCESS(200, "处理成功"),
    ERROR(500, "服务器内部错误"),
    PERMISSION_DENIED_ERROR(500, "暂无权限访问"),
    AUTHENTICATION_ERROR(401, "用户名或者密码错误，请重新输入"),
    AUTHENTICATION_INFO_ERROR(401, "凭据信息错误，请重新认证"),
    AUTHENTICATION_INFO_EXPIRED(401, "凭据信息过期或错误，请重新认证"),
    AUTHENTICATION_IS_REQUIRED(401, "凭据信息不能为空，请重新认证"),
    AUTHENTICATION_INFO_RESOURCE(401, "访问此资源需要身份验证"),

    /**
     * Config use 100001 ~ 100999.
     **/
    REGISTER_USER_ERROR(100000, "注册用户信息失败，请重新注册"),
    ARGUMENT_VALID_ERROR(100001, "输入的参数有误"),
    CONNECT_TIMEOUT_ERROR(100002, "服务连接超时"),
    NOT_KNOWN_INDEX_ERROR(100003, "找不到相关的索引"),
    USERNAME_EXIST_ERROR(100004, "用户名已存在"),
    NOT_SUPPORTED_REQUEST_METHOD(100005, "接口不支持的请求方法"),
    ROLE_REPEAT_ERROR(100006, "名称或英文名称重复"),
    ROLE_NOT_DELETE_ERROR(100007, "此角色不能删除"),
    REQUEST_MISSING_BODY_ERROR(100008, "请求缺少正文body"),
    DICTIONARY_REPEAT_ERROR(100009, "数据字典value值已经存在"),
    DELETE_CHILDREN_ERROR(100010, "数据存在子级，不可删除"),
    APPROVAL_SUGGEST_NOTNULL_ERROR(100011, "审核意见不能为空"),
    USER_LEVEL_ERROR(100012, "未登录或您的等级权限不能查看此文献内容"),
    DOCUMENT_EXIST_ERROR(100013, "此文档不存在或被删除"),
    REGISTER_USER_INFO_ERROR(100014, "注册用户信息有误，请重新注册"),
    PASSWORD_DIFFERENT_ERROR(100015, "输入的旧密码错误，请重新输入"),
    ;

    ResultCodeEnum(int code, String codeMsg) {
        this.code = code;
        this.msg = codeMsg;
    }

    private final int code;
    private final String msg;
}

