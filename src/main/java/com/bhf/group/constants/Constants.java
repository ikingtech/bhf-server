package com.bhf.group.constants;

import java.io.Serializable;

/**
 * @author pwq
 */
public class Constants implements Serializable {
    private static final long serialVersionUID = 814738392230165746L;

    /**
     * 附件上传BUCKETS
     */
    public static final String BUCKETS_NAME = "file-bf";

    /**
     * BHF_DATA_INDEX
     */
    public static final String BHF_DATA_INDEX = "bhf-data-index";

    /**
     * 验证码Key
     */
    public static final String LOGIN_CAPTCHA = "register:captcha:";

    /**
     * 用户jwt标识
     */
    public static final String USER_IDENTIFY = "user:login:identify:";
    public static final String USER_FREEZE = "user:freeze:";
    public static final String IDENTIFY = "identify";
    public static final String CODE = "code";
    public static final String MSG = "message";
    public static final String JTI = "jti";

    /**
     * 系统默认角色
     */
    public static final String[] ROLES = {"root", "admin", "user", "dataAdmin"};


    public static final String DATA_CATEGORY = "document:category";
    public static final String DATA_CATEGORY_DETAILS = DATA_CATEGORY + ":details";
    public static final String DATA_CATEGORY_ID = DATA_CATEGORY + ":data:";

    public static final String STOP_KEWS= "sys:stop";
    public static final String EXTEND_KEWS= "sys:extend";
    public static final String HOTS_KEWS= "sys:hots";

    public static final String NEWS = "news";
}

