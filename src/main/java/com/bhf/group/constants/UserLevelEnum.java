package com.bhf.group.constants;

import lombok.Getter;

/**
 * @author pwq
 */

@Getter
public enum UserLevelEnum {

    OPEN(0, "open", "开放"),
    INSIDE(1, "inside", "内部"),
    SECRET(2, "secret", "秘密"),
    CONFIDENTIAL(3, "confidential", "机密"),
    MOST_CONFIDENTIAL(4, "most-confidential", "绝密"),
    ;

    UserLevelEnum(int grade, String value, String msg) {
        this.grade = grade;
        this.value = value;
        this.msg = msg;
    }

    private final int grade;
    private final String value;
    private final String msg;

    public static int getGrade(String val) {
        for (UserLevelEnum value : UserLevelEnum.values()) {
            if (value.getValue().equals(val)) {
                return value.getGrade();
            }
        }
        return 0;
    }
}
