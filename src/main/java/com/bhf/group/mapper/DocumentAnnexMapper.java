package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bhf.group.entity.DocumentAnnex;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * @author pwq
 */
@Mapper
public interface DocumentAnnexMapper extends BaseMapper<DocumentAnnex> {

    @Update("UPDATE document_annex SET nums = nums+1 WHERE id = #{id}")
    void download(String id);
}
