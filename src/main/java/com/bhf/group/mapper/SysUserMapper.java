package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.entity.SysUser;
import com.bhf.group.security.entity.RoleInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    @Select("select sr.name,sr.id from sys_user_role sur left join sys_role sr on sur.role_id=sr.id and sr.status=0 where sur.user_id=#{id}")
    List<RoleInfo> getRoles(String id);


    @Select("select convert(varchar(40),ucd.id) as id,convert(varchar(40),ucd.document_id) as documentId,doc.title,ucd.create_time as createTime,su.username,doc.likes,doc.previews,\n" +
            "doc.collects,doc.summary from user_collect_document ucd left join document doc on ucd.document_id=doc.id left join sys_user su on doc.user_id=su.id ${ew.customSqlSegment}")
    Page<Map<String, String>> getUserCollectsList(Page<Object> page, @Param(Constants.WRAPPER) Wrapper<Map<String, String>> wrapper);

    @Select("select convert(varchar(40),ubd.id) as id,convert(varchar(40),ubd.document_id) as documentId,doc.title,ubd.create_time as createTime,su.username,doc.likes,doc.collects,\n" +
            "doc.previews,doc.summary from user_browse_document ubd left join document doc on ubd.document_id=doc.id left join sys_user su on doc.user_id=su.id ${ew.customSqlSegment}")
    Page<Map<String, String>> getUserPreviewsList(Page<Object> page, @Param(Constants.WRAPPER) Wrapper<Map<String, String>> wrapper);
}
