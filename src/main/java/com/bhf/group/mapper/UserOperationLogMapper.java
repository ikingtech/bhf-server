package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.bhf.group.entity.UserOperationLog;
import com.bhf.group.mapper.builder.UserOperationLogBuilder;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.Map;

/**
 * 采用mybatis新特性java api形式,不在采用xml形式
 *
 * @author pwq
 */
@Mapper
public interface UserOperationLogMapper extends BaseMapper<UserOperationLog> {

    /**
     * 查询操作日志记录
     *
     * @param page
     * @param wrapper
     * @return
     */
    @SelectProvider(type = UserOperationLogBuilder.class, method = "getUserOperationLogs")
    IPage<Map<String, Object>> getUserOperationLogs(IPage<Map<String, Object>> page, @Param(Constants.WRAPPER) Wrapper<Map<String, Object>> wrapper);

}
