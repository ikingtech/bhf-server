package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bhf.group.entity.DocumentTemplate;
import com.bhf.group.entity.DocumentTemplateOptions;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @author pwq
 */
@Mapper
public interface DocumentTemplateMapper extends BaseMapper<DocumentTemplate> {

    /**
     * 查询模板信息
     *
     * @return list
     */
    @Results(id = "studentResult", value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "name", column = "name"),
            @Result(property = "categoryId", column = "category_id"),
            @Result(property = "type", column = "type"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "status", column = "status"),
            @Result(property = "required", column = "required"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "updateTime", column = "update_time"),
            @Result(property = "valueData", javaType = List.class, many = @Many(select = "selectDocumentTemplateOptionsById"), column = "id")
    })
    @Select("select * from document_template where category_id=#{categoryId} and status=0 order by sort asc")
    List<DocumentTemplate> getDocumentTemplateList(String categoryId);

    /**
     * 查询模板对应的options
     *
     * @param id -
     * @return map
     */
    @Select("select value,label,document_template_id from document_template_options where document_template_id=#{id}")
    List<DocumentTemplateOptions> selectDocumentTemplateOptionsById(@Param("id") Long id);
}
