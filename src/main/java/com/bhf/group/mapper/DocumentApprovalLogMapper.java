package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.entity.DocumentApprovalLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.Map;
import java.util.Objects;

/**
 * @author pwq
 */
@Mapper
public interface DocumentApprovalLogMapper extends BaseMapper<DocumentApprovalLog> {

    @Select("select dal.id,su.username,dal.suggest,dal.create_time as createTime,dal.approval_status as approvalStatus from document_approval_log dal " +
            "left join sys_user su on dal.user_id=su.id ${ew.customSqlSegment}")
    Page<Map<String, Object>> getDocumentApprovalLogList(Page<Object> objectPage, @Param(Constants.WRAPPER) QueryWrapper<Map<String, Objects>> wrapper);

    @Select("select su.username from document_approval_log dal left join sys_user su on dal.user_id=su.id where dal.approval_status=2 and dal.document_id=#{id}")
    String getApprovalUserName(String id);
}
