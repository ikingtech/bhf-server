package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.entity.SysNews;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Map;

/**
 * @author pwq
 */
@Mapper
public interface SysNewsMapper extends BaseMapper<SysNews> {
    @Update("UPDATE sys_news SET previews = previews+1 WHERE status = 0 and type=1 and id=#{id} ")
    void updateNewsPreviews(String id);

    @Select("select sn.id,sn.title,sn.content,su.username as create_user,sn.previews,sn.publish_time,sn.create_time\n" +
            "from sys_news sn left join sys_user su on sn.create_user=su.id ${ew.customSqlSegment}")
    SysNews selectOneById(@Param(Constants.WRAPPER) Wrapper<Map<String, Object>> wrapper);

    @Select("select sn.id,sn.title,sn.content,su.username as create_user,sn.previews,sn.publish_time,sn.create_time\n" +
            "from sys_news sn left join sys_user su on sn.create_user=su.id ${ew.customSqlSegment}")
    Page<Map<String, Object>> getNewsList(Page<Object> page, @Param(Constants.WRAPPER) QueryWrapper<Map<String, Object>> wrapper);
}
