package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bhf.group.entity.DocumentTemplateCategory;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author pwq
 */
@Mapper
public interface DocumentTemplateCategoryMapper extends BaseMapper<DocumentTemplateCategory> {
}
