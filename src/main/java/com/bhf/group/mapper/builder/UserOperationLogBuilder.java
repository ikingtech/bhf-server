package com.bhf.group.mapper.builder;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

/**
 * @author pwq
 */
public class UserOperationLogBuilder {

    public static String getUserOperationLogs(final Wrapper<Map<String, Object>> ew) {
        return new SQL() {{
            SELECT("upl.id,upl.operation,upl.ip,upl.result_info as resultInfo,upl.time_consuming as timeConsuming," +
                    "upl.create_time as createTime,upl.method,su.username");
            FROM("user_operation_log upl");
            LEFT_OUTER_JOIN("sys_user su on upl.user_id = su.id and su.status=0 ${ew.customSqlSegment}");
        }}.toString();
    }
}
