package com.bhf.group.mapper.builder;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import org.apache.ibatis.jdbc.SQL;

import java.util.Map;

/**
 * @author pwq
 */
public class SysUserMapperBuilder {

    public static String getUserLists(final Wrapper<Map<String, Object>> ew) {
        return new SQL() {{
            SELECT("id,username,department,status,signature,unit," +
                    "create_time,update_time,avatar,user_level,sex");
            FROM("sys_user ${ew.customSqlSegment}");
        }}.toString();
    }
}
