package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bhf.group.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    @Select("select sr.id,sr.name from sys_user_role sur left join sys_role sr on sur.role_id=sr.id where sr.status=0 and sur.user_id = #{id}")
    List<Map<String, Object>> getUserRole(String id);
}
