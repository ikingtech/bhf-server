package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bhf.group.entity.DocumentCategory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * @author pwq
 */
@Mapper
public interface DocumentCategoryMapper extends BaseMapper<DocumentCategory> {

    @Update("UPDATE document_category SET nums = nums-1 WHERE id = #{id} ")
    void subDocumentCategoryNums(String id);
}
