package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bhf.group.entity.SysExtendWord;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author pwq
 */
@Mapper
public interface SysExtendWordMapper extends BaseMapper<SysExtendWord> {
}
