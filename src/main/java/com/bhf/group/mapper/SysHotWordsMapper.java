package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bhf.group.entity.SysHotWords;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Update;

/**
 * @author pwq
 */
@Mapper
public interface SysHotWordsMapper extends BaseMapper<SysHotWords> {

    @Update("UPDATE sys_hot_words SET nums = nums+1 WHERE id = #{id}")
    void updateNums(String id);
}
