package com.bhf.group.mapper;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bhf.group.entity.Document;
import com.bhf.group.entity.DocumentAnnex;
import com.bhf.group.entity.DocumentValue;
import com.bhf.group.entity.DocumentValueOptions;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * @author pwq
 */
@Mapper
public interface DocumentMapper extends BaseMapper<Document> {


    @Results(value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "documentCategoryId", column = "document_category_id"),
            @Result(property = "title", column = "title"),
            @Result(property = "likes", column = "likes"),
            @Result(property = "keyWords", column = "key_words"),
            @Result(property = "status", column = "status"),
            @Result(property = "summary", column = "summary"),
            @Result(property = "collects", column = "collects"),
            @Result(property = "level", column = "level"),
            @Result(property = "isFreeze", column = "is_freeze"),
            @Result(property = "custom", column = "custom"),
            @Result(property = "coverImage", column = "cover_image"),
            @Result(property = "templateId", column = "template_id"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "updateTime", column = "update_time"),
            @Result(property = "documentAnnexList", javaType = List.class, many = @Many(select = "selectDocumentAnnexListById"), column = "id"),
            @Result(property = "documentValues", javaType = List.class, many = @Many(select = "selectDocumentValuesListById"), column = "id")
    })
    @Select("select * from document ${ew.customSqlSegment}")
    Page<Document> getDocumentPageList(Page<Object> page, @Param(Constants.WRAPPER) Wrapper<Document> wrapper);

    @Select("select id,document_id,annex_url,name,sort,create_time,type,nums from document_annex where document_id = #{id} order by sort asc")
    List<DocumentAnnex> selectDocumentAnnexListById(@Param("id") Long id);

    @Select("select distinct type from document_annex where document_id = #{id}")
    List<String> selectDocumentAnnexTypeListById(@Param("id") Long id);


    @Results(value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "type", column = "type"),
            @Result(property = "documentId", column = "document_id"),
            @Result(property = "value", column = "value"),
            @Result(property = "required", column = "required"),
            @Result(property = "name", column = "name"),
            @Result(property = "custom", column = "custom"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "sort", column = "sort"),
            @Result(property = "valueOptions", javaType = List.class, many = @Many(select = "selectValueOptionsListById"), column = "id")
    })
    @Select("select id,create_time,document_id,type,value,sort,name,required,custom from document_value where document_id = #{id}")
    List<DocumentValue> selectDocumentValuesListById(@Param("id") Long id);

    @Select("select id,label,value,selected,document_value_id from document_value_options where document_value_id = #{id}")
    List<DocumentValueOptions> selectValueOptionsListById(@Param("id") Long id);


    @Results(value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "title", column = "title"),
            @Result(property = "likes", column = "likes"),
            @Result(property = "keyWords", column = "key_words"),
            @Result(property = "summary", column = "summary"),
            @Result(property = "collects", column = "collects"),
            @Result(property = "username", column = "username"),
            @Result(property = "previews", column = "previews"),
            @Result(property = "coverImage", column = "cover_image"),
            @Result(property = "documentAnnexList", javaType = List.class, many = @Many(select = "selectDocumentAnnexTypeListById"), column = "id")
    })
    @Select("select convert(varchar(20),doc.id) as id,doc.title,doc.likes,doc.collects,doc.key_words,su.username,doc.cover_image" +
            ",doc.summary,doc.previews,doc.publish_time as publishTime from document doc left join sys_user su on doc.user_id=su.id ${ew.customSqlSegment}")
    Page<Map<String, Object>> findIndexDocumentData(Page<Object> tPage, @Param(Constants.WRAPPER) Wrapper<Map<String, Object>> wrapper);

    @Results(value = {
            @Result(property = "id", column = "id", id = true),
            @Result(property = "title", column = "title"),
            @Result(property = "likes", column = "likes"),
            @Result(property = "keyWords", column = "key_words"),
            @Result(property = "status", column = "status"),
            @Result(property = "summary", column = "summary"),
            @Result(property = "collects", column = "collects"),
            @Result(property = "level", column = "level"),
            @Result(property = "coverImage", column = "cover_image"),
            @Result(property = "templateId", column = "template_id"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "updateTime", column = "update_time"),
            @Result(property = "documentAnnexList", javaType = List.class, many = @Many(select = "selectDocumentAnnexListById"), column = "id"),
            @Result(property = "documentValues", javaType = List.class, many = @Many(select = "selectDocumentValuesListById"), column = "id")
    })
    @Select("select * from document ${ew.customSqlSegment}")
    Document getDocumentDetailsById(@Param(Constants.WRAPPER) Wrapper<Document> wrapper);

    @Update("UPDATE document SET likes = likes+#{count} WHERE id = #{id} ")
    void updateDocumentLike(@Param("id") String id, @Param("count") int count);

    @Update("UPDATE document SET collects = collects+#{count} WHERE id = #{id} ")
    void updateDocumentCollect(String id, int count);

    @Update("UPDATE document SET previews = previews+#{count} WHERE id = #{id} ")
    void updateDocumentPreviews(String id, int count);
}
