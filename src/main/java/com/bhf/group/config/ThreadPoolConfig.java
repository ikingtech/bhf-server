package com.bhf.group.config;

import com.bhf.group.core.CustomThreadPoolTaskExecutor;
import com.bhf.group.core.TaskThreadPoolConfigs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.annotation.Resource;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author pwq
 */
@Slf4j
@Configuration
@EnableAsync
public class ThreadPoolConfig implements AsyncConfigurer {

    @Resource
    private TaskThreadPoolConfigs configs;

    @Override
    public Executor getAsyncExecutor() {
        CustomThreadPoolTaskExecutor executor = new CustomThreadPoolTaskExecutor();
        //核心线程池大小
        executor.setCorePoolSize(configs.getCorePoolSize());
        //队列容量
        executor.setMaxPoolSize(configs.getMaxPoolSize());
        //队列容量
        executor.setQueueCapacity(configs.getQueueCapacity());
        //活跃时间
        executor.setKeepAliveSeconds(configs.getKeepAliveSeconds());
        //线程名字前缀
        executor.setThreadNamePrefix("async-thread-");
        //当poolSize已达到maxPoolSize，如何处理新任务（是拒绝还是交由其它线程处理）
        //CallerRunsPolicy：不在新线程中执行任务，而是由调用者所在的线程来执行
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return (ex, method, params) -> {
            log.error("==========================" + ex.getMessage() + "=======================", ex);
            log.error("exception method:" + method.getName());
        };
    }
}
