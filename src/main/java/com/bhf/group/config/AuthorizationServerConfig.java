package com.bhf.group.config;

import com.bhf.group.security.CustomTokenGranter;
import com.bhf.group.security.exception.CustomWebResponseExceptionTranslator;
import com.bhf.group.security.handler.CustomJwtAccessTokenConverter;
import com.bhf.group.security.handler.CustomTokenEnhancer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.CompositeTokenGranter;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.approval.JdbcApprovalStore;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeTokenGranter;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.implicit.ImplicitTokenGranter;
import org.springframework.security.oauth2.provider.password.ResourceOwnerPasswordTokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author pwq
 */
@Configuration
@EnableAuthorizationServer
@Order(2)
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    public static final String SIGNING_KEY = "NTg2I2Rkc3dlNjMuNiMyNDI5MlJ3ZGFkZappa2p3c3dkMjM";

    @Resource
    private DataSource dataSource;
    @Resource
    private AuthenticationManager authenticationManagerBean;
    @Resource
    private UserDetailsService userDetailsService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.jdbc(dataSource);
    }

    @Bean
    public TokenStore jwtTokenStore() {
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        converter.setSigningKey(SIGNING_KEY);
        converter.setAccessTokenConverter(new CustomJwtAccessTokenConverter());
        return converter;
    }

    @Bean
    public TokenEnhancer customTokenEnhancer() {
        return new CustomTokenEnhancer(redisTemplate);
    }


    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
        endpoints.tokenStore(jwtTokenStore());
        TokenEnhancerChain chain = new TokenEnhancerChain();
        chain.setTokenEnhancers(Arrays.asList(customTokenEnhancer(), jwtAccessTokenConverter()));
        endpoints.tokenEnhancer(chain);
        endpoints.accessTokenConverter(jwtAccessTokenConverter());
        endpoints.approvalStore(new JdbcApprovalStore(dataSource));
        endpoints.authenticationManager(authenticationManagerBean);
        endpoints.userDetailsService(userDetailsService);
        endpoints.authorizationCodeServices(new JdbcAuthorizationCodeServices(dataSource));
        endpoints.allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST);
        endpoints.exceptionTranslator(new CustomWebResponseExceptionTranslator());
        endpoints.tokenGranter(tokenGranter(endpoints));
    }

    private TokenGranter tokenGranter(AuthorizationServerEndpointsConfigurer e) {
        List<TokenGranter> tokenGranters = new ArrayList<>();
        tokenGranters.add(new AuthorizationCodeTokenGranter(e.getTokenServices(),
                e.getAuthorizationCodeServices(), e.getClientDetailsService(), e.getOAuth2RequestFactory()));
        tokenGranters.add(new RefreshTokenGranter(e.getTokenServices(), e.getClientDetailsService(), e.getOAuth2RequestFactory()));
        ImplicitTokenGranter implicit = new ImplicitTokenGranter(e.getTokenServices(), e.getClientDetailsService(), e.getOAuth2RequestFactory());
        tokenGranters.add(implicit);
        tokenGranters.add(new ClientCredentialsTokenGranter(e.getTokenServices(), e.getClientDetailsService(), e.getOAuth2RequestFactory()));
        tokenGranters.add(new CustomTokenGranter(e.getTokenServices(), e.getClientDetailsService(),
                e.getOAuth2RequestFactory(), authenticationManagerBean, redisTemplate));
        if (authenticationManagerBean != null) {
            tokenGranters.add(new ResourceOwnerPasswordTokenGranter(authenticationManagerBean, e.getTokenServices(), e.getClientDetailsService(), e.getOAuth2RequestFactory()));
        }
        return new CompositeTokenGranter(tokenGranters);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) {
        security.tokenKeyAccess("permitAll()").checkTokenAccess("permitAll()").allowFormAuthenticationForClients();
    }
}
