package com.bhf.group.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author pwq
 */
@Configuration
public class CorsConfig implements WebMvcConfigurer {

    private static final String[] METHOD = {HttpMethod.GET.name(), HttpMethod.POST.name(),
            HttpMethod.HEAD.name(), HttpMethod.OPTIONS.name(),
            HttpMethod.DELETE.name(), HttpMethod.PUT.name()};
    private static final String ALL = "*";

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedMethods(METHOD)
                .allowedHeaders(ALL)
                .allowedOrigins(ALL)
                .allowCredentials(true)
                .maxAge(3600);
    }
}
