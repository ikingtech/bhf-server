package com.bhf.group.config;

import com.bhf.group.security.CustomResourceServerTokenServices;
import com.bhf.group.security.handler.CustomAccessDeniedHandler;
import com.bhf.group.security.point.CustomAuthenticationEntryPoint;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.token.TokenStore;

import javax.annotation.Resource;

/**
 * @author pwq
 */
@Configuration
@EnableResourceServer
@Order(3)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Resource
    private TokenStore jwtTokenStore;
    @Resource
    private ClientDetailsService clientDetailsService;
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources.stateless(true);
        resources.authenticationEntryPoint(new CustomAuthenticationEntryPoint());
        resources.accessDeniedHandler(new CustomAccessDeniedHandler());
        resources.tokenServices(new CustomResourceServerTokenServices(jwtTokenStore, clientDetailsService, redisTemplate));
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //不需要认证的接口
                .antMatchers("/doc.html", "/**/*.css", "/**/*.js", "/swagger-resources", "/**/api-docs").permitAll()
                .antMatchers("/register/**", "/index/**").permitAll()
                .anyRequest()
                /*.access("");*/
                .authenticated();
    }
}
