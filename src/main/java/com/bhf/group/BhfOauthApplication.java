package com.bhf.group;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author pwq
 */
@EnableTransactionManagement
@SpringBootApplication(proxyBeanMethods = false)
public class BhfOauthApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(BhfOauthApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(BhfOauthApplication.class);
    }
}
